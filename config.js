module.exports = {
	database: (function() {
		// the database settings
		var uri = '';	// mongodb server
		var port = '27017'; // mongodb port
		var database = '';  // mongodb database
		var username = "";	// mongodb user account
		var password = "";	// mongodb user account password
		return {
			uri: uri
			, port: port
			, database: database
			, username: username
			, password: password
			, connectionString: 'mongodb://' + username + ':' + password + '@' + uri + ":" + port + "/" + database
		}
	})()
	, web: {
		// the port that the application will run on
		port: process.env.PORT || 8082
		,"tokenSecret": "sampleToken"	// the secret for the JSON web token
		,"flightBase": "http://ghgdata.epa.gov/ghgp/service"	// the base address for the GHG service
	}
};