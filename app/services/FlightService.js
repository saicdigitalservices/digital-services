// CCBService.js - in api/services
var Promise = require('bluebird');

module.exports = {
  /**
   * Standard method to perform GET calls on the Greenhouse Gas FLIGHT service
   * Service is the service method that will be called in GHG
   * Parameters is a JSON list of parameters to be included in the querystring
   */
  getData: function(service, parameters) {
    var http = require('http');
    var querystring = require('querystring');

    var query = querystring.stringify(parameters);

    var options = {
      hostname: "ghgdata.epa.gov",
      port: 80,
      path: "/ghgp/service/" + service + "?" + query,
      method: "GET"
    };

    // everything is returned as a promise
    return new Promise(function(resolve, reject) {

      var request = http.request(options, function (response) {
        var responseData = '';
        response.setEncoding('utf8');

        response.on('data', function (chunk) {
          responseData += chunk;
        });

        response.once('error', function (err) {
          // Some error handling here, e.g.:
          //res.serverError(err);
        });

        response.on('end', function () {
          try {
            // the results are returned as JSON data
			      return resolve(JSON.parse(responseData));
          } catch (e) {
            console.log.warn('Could not parse response from options.hostname: ' + e);
          }
        });
      });

      request.end();
    });
  },
  /**
   * Return the national census data
   */
  getNationalCensusData: function() {
    return {
      TOTALPOP: 309138711,
      HSHOLDS: 115226802,
      PERC_MINOR: 36.3,
      PERC_BELOW12: 14.3,
      PERC_HS_DG: 28.2,
      PERC_COL_DG: 28.5,
      PERC_AGE_UNDER5: 6.5,
      PERC_AGE_UNDER18: 23.9,
      PERC_AGE_OVER64: 13.2,
      PERC_HOME_PRE50: 19.3,
      PERC_LAN_ENG_LTW: 4.7,
      PERC_FEMALE: 50.8,
      PERC_RENTERS: 34.5
    };
  },
  /**
   * Call the EJSCREEN service to get the census information for a county
   * countyFIPS is the fips code for the county
   */
  getCensusData: function(countyFIPS) {
    var https = require('https');
    
    var hostname = "ejscreen.epa.gov";
    var path = "/arcgis/rest/services/ejscreen/census2012acs/MapServer/2/query?where=stcn%3D%27" + countyFIPS + "%27&outFields=TOTALPOP,HSHOLDS,PERC_MINOR,PERC_BELOW12,PERC_HS_DG,PERC_COL_DG,PERC_AGE_UNDER5,PERC_AGE_UNDER18,PERC_AGE_OVER64,PERC_HOME_PRE50,PERC_LAN_ENG_LTW,PERC_FEMALE,PERC_RENTERS&returnGeometry=false&f=json"; 

    var options = {
      hostname: hostname,
      port: 443,
      path: path,
      method: "GET"
    };

    return new Promise(function(resolve, reject) {

      var request = https.request(options, function (response) {
        var responseData = '';
        response.setEncoding('utf8');

        response.on('data', function (chunk) {
          responseData += chunk;
        });

        response.once('error', function (err) {
          // Some error handling here, e.g.:
          //res.serverError(err);
        });

        response.on('end', function () {
          try {
            // return the results as JSON
			      return resolve(JSON.parse(responseData));
          } catch (e) {
            console.log.warn('Could not parse response from options.hostname: ' + e);
          }
        });
      });

      request.end();
    });
  },
  /**
   * A generic post method to call services in Greenhouse Gas that require POST calls
   * Service is the service method that will be called in GHG
   * Parameters is a JSON list of parameters to be included in the querystring
   * PostData is the body of the post request (should be a JSON list)
   */
  postData: function(service, parameters, postData) {
    var http = require('http');
    var querystring = require('querystring');

    var query = querystring.stringify(parameters);
    var postDataString = JSON.stringify(postData);

    var postOptions = {
      hostname: "ghgdata.epa.gov",
      port: 80,
      path: "/ghgp/service/" + service + "?" + query,
      method: "POST",
      headers: {
          'Content-Type': 'application/json; charset=utf-8',
          'Content-Length': Buffer.byteLength(postDataString)
      }
    };

    // everything is returned as a promise
    return new Promise(function(resolve, reject) {

      var request = http.request(postOptions, function (response) {
        var responseData = '';
        response.setEncoding('utf8');

        response.on('data', function (chunk) {
          responseData += chunk;
        });

        response.once('error', function (err) {
          // Some error handling here, e.g.:
          //res.serverError(err);
        });

        response.on('end', function () {
          try {
            // the results are made into a JSON object
			      return resolve(JSON.parse(responseData));
          } catch (e) {
            console.log('Error: Could not parse response: ' + e);
          }
        });
      });
      request.write(postDataString);
      request.end();
    });
  }
};