/**  * Created by alabdullahwi on 12/16/2015.
 *
 * database service which connects to mongo to load the heat map data
 *
 */
var mongodb = require('mongodb')
  ,Server = mongodb.Server
  ,Db = mongodb.Db
  ;
var mongoClient = require('mongodb').MongoClient;

var config = require('../../config').database;
var Promise = require('bluebird');

//a representation of the facilities collection in mongodb
var facilities;

/**
 * returns the facilities emissions data from the mongo database in a JSON array to be used in the heat map calculations
 */
var loadAll = function() {
    return new Promise(function( resolve,reject) {
        if (facilities) { return resolve ({ status: 'success' , body: facilities } );}
        else {
            connect().then(function() {
                return resolve({ status: 'success' , body: facilities } );
            });
        }
    });
}

/**
 * loads the heatmap data from the mongo database and finds all of the facilities that have at least some emmisions and coordinates
 */
var loadHeatmapData = function() {
    return new Promise(function(resolve,reject) {
        loadAll().then(
            function() {
                return resolve({status: 'success', body: facilities.find().project({totalEmissions: 1, coordinates: 1})})
            }
        );
    });
}

/**
 * connects ot the mongo database and returns the information in the facilities collection
 */
var connect = function() {

    return new Promise(function(resolve, reject) {
mongoClient.connect(config.connectionString, function(err, db) {
  console.log("Connected correctly to server");
  db.collection('facilities', function(err, coll) {
                    if (err) {return reject( { status: 'error' , body:  err } );}
                    facilities = coll;
                    return resolve( { status: 'success', body: facilities } );
                });
});
});
};


module.exports = {
    connect : connect
    ,loadAll: loadAll
    ,loadHeatmapData : loadHeatmapData
};
