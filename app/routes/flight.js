/****************
 * Sets up the routes for querying data sources, any function before the authentication block will be publicly available, everything afterwards requires an authentication token 
 ***************/
var config = require('../../config');
var Promise = require('bluebird');
var http = require("http");
var FlightService = require('../services/FlightService');
var jwt = require('jsonwebtoken');
var _ = require('lodash');

module.exports = function(app, express) {
	var flightRouter = express.Router();
	
	// ordered dictionary of the sectors, used to set the properties for calls to the GHG service
	var sectorDictionary = [
		"Power Plants",
		"Waste",
		"Metals",
		"Minerals",
		"Refineries",
		"Pulp and Paper",
		"Chemicals",
		"Other",
		"Petroleum and Natural Gas Systems"
	];

	// test route to make sure everything is working 
	// accessed at GET http://localhost:8080/flight
	flightRouter.get('/', function(req, res) {
		res.json({ message: 'hooray! welcome to our api!' });	
	});
	
	/**
	 * Ping method to make sure that the service is up and running
	 */
	flightRouter.get('/ping', function(req, res) {
		res.json({status: new Date()});
	});
	
	/**
	 * Get the sectors from GHG (include the County FIPS code as a "county parameter" to get localized data)
	 */
	flightRouter.get("/getGHGSectors", function(req, res) {
		console.log("In national ghg sectors method");
		// data to include in the post, the first item in a sectors array is turned on and off to turn on a sector if it should be included in the results, if a county is specified, countyFips is set
		var nationalPostData = {
			"trend":"current", 
			"dataSource":"E", 
			"reportingYear":"2014", 
			"query":"",
			"lowE":"0", 
			"highE":"23000000", 
			"state":"",
			"countyFips":"",
			"msaCode":"",
			"stateLevel":0,
			"basin":"",
			"gases":[true,true,true,true,true,false,true,true,true,true],
			"sectors":[[true],[true,true,true,true,true],[true,true,true,true,true,true,true,true],[true,true,true,true,true,true],[true],[true,true,true],[true,true,true,true,true,true,true,true,true,true,true,true],[true,true,true,true,true,true,true,true,true,true,true],[true,true,true,true,true,true,true,true,true,true]],
			"sortOrder":0,
			"supplierSector":0,
			"reportingStatus":"ALL",
			"searchOptions":"11001000",
			"injectionSelection":11,
			"emissionsType":"",
			"tribalLandId":"",
			"pageNumber":0
		};
		
		// if the county is provided, put in the post as the countyFips
		if(req.query.county !== undefined) {
			nationalPostData.countyFips = req.query.county;
		}
		
		// call the populateSectorDashboard method
		FlightService.postData("populateSectorDashboard", {}, nationalPostData).then(function(results) {
			// parse the results and format into a more descriptive object with the sector names
			var returnResults = {};
			returnResults.dataValues = [];
			// parse the results and set the data values to be pushed, use the sectorDictionary to figure out the name for each sector based on the order of the returned results
			for (var i = 0; i < results.values.length; i++) {
				if(i % 2 === 0) {
					var dataPoint = {};
					dataPoint.name = sectorDictionary[i / 2];
					dataPoint.value = results.values[i];
					// only add entries where there is a value provided
					if(dataPoint.name !== undefined) {
						returnResults.dataValues.push(dataPoint);
					}
				}
			}
			
			// sort the results by value
			returnResults.dataValues = _.sortBy(returnResults.dataValues, "value");
			
            returnResults.units = results.unit;
            
			res.json(returnResults);
		});
	});
	
	/**
	 * Get the top 5 facilities for a sector in the nation or a county (if county, the FIPS code is set in the county querystring parameter)
	 */
	flightRouter.get("/getSectorTopFacilities", function(req, res) {
		console.log("In national ghg sectors top facilities method");
		// data to include in the post, the first item in a sectors array is turned on and off to turn on a sector if it should be included in the results, if a county is specified, countyFips is set
		var nationalPostData = {
			"basin":"",
			"countyFips":"",
			"currentYear":2014,
			"dataSource":"E",
			"emissionsType":"",
			"gases":[true,true,true,true,true,false,true,true,true,true],
			"highE":"23000000",
			"injectionSelection":11,
			"lowE":"0",
			"msaCode":"",
			"pageNumber":null,
			"query":"",
			"reportingStatus":"ALL",
			"reportingYear":"2014",
			"searchOptions":"11001000",
			"sectors":[[false],[false,true,true,true,true],[false,true,true,true,true,true,true,true],[false,true,true,true,true,true],[false],[false,true,true],[false,true,true,true,true,true,true,true,true,true,true,true],[false,true,true,true,true,true,true,true,true,true,true],[false,true,true,true,true,true,true,true,true,true]],
			"sortOrder":"0",
			"state":"",
			"stateLevel":0,
			"supplierSector":0,
			"trend":"current",
			"tribalLandId":""
		};
		
		// set the selected sector
		if(req.query.sector !== undefined && req.query.sector.length > 0) {
			var sectorId = _.indexOf(sectorDictionary, req.query.sector);
			
			nationalPostData.sectors[sectorId][0] = true;
		}
		
		// if a county is provided, set the countyFips property
		if(req.query.county !== undefined && req.query.sector.length > 0) {
			nationalPostData.countyFips = req.query.county;
		}
		
		FlightService.postData("/listFacility/US", {}, nationalPostData).then(function(results) {
			// parse the results and format into a more descriptive object with the sector names
			var returnResults = {};
			if(results !== undefined && results.data !== undefined && results.data.rows !== undefined) {
				// make sure the numbers are cleaned and converted to integers so they can be sorted
				_.forEach(results.data.rows, function(n) {
					if(n.total != "---") {
						n.totalNumber = parseInt(n.total.replace(/,/g, ""));
					}
					else {
						n.totalNumber = -1;
					}
				});
				// sort by the totalNumber attribute and then get the top 5 results
				var facilityResults = _.orderBy(results.data.rows, ["totalNumber"], ["desc"]);
				var maxLength = 5;
				if(facilityResults.length <= 5) {
					maxLength = facilityResults.length;
				}
				returnResults.facilityData = _.slice(facilityResults, 0, maxLength); 
			}
            
            returnResults.units = results.unit;
			
			res.json(returnResults);
		});
	});
		
	/**
	 * Get the census data for a county based on a county fips code (county parameter)
	 */
	flightRouter.get('/getCensusData', function(req, res) {
		console.log("In get census data method");
		if(req.query.county !== undefined && req.query.county.length > 0)
		FlightService.getCensusData(req.query.county).then(function(results) {
			res.json(results);
		});
	});
	
	/**
	 * Get the national census data
	 */
	flightRouter.get('/getNationalCensusData', function(req, res) {
		console.log("In get national census data method");
		res.json(FlightService.getNationalCensusData());
	});
	
	////Authentication///////////////
	
	// route to authenticate a user (POST http://localhost:8080/flight/authenticate)
	flightRouter.post("/authenticate", function(req, res) {
		var password = req.body.password;
		if (password !== "P@ssw0rd") {
			 res.json({ 
				success: false, 
				message: 'Authentication failed.' 
			});
		}
		else {
			// can authenticate and then sign a token
			var token = jwt.sign({
        		name: "approvedUser"
        	}, config.web.tokenSecret, {
          		expiresInMinutes: 1440 // expires in 24 hours
        	});
			
			// return the information including token as JSON
			res.json({
			success: true,
			message: 'Enjoy your token!',
			token: token
			});
		}
	});

	// route middleware to verify a token
	flightRouter.use(function(req, res, next) {
		// check header or url parameters or post parameters for token
		var token = req.body.token || req.query.token || req.headers['x-access-token'];
		
		// decode token
		if (token) {
		
			// verifies secret and checks exp
			jwt.verify(token, config.tokenSecret, function(err, decoded) {      
				if (err) {
					return res.json({ success: false, message: 'Failed to authenticate token.' });   
				} else {
					// if everything is good, save to request for use in other routes
					req.decoded = decoded;    
					next(); // make sure we go to the next routes and don't stop here
				}
			});
		} else {
			// if there is no token
			// return an HTTP response of 403 (access forbidden) and an error message
			return res.status(403).send({ 
			success: false, 
			message: 'No token provided.' 
			});
		}
	});
	/////////Authentication////////////

	/**
	 * Sample method to make sure that the user is successfully authenticated
	 */
	flightRouter.get('/checkAuthenticated', function(req, res) {
		res.json({status: new Date()});
	});

	return flightRouter;
};