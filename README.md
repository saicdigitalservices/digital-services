# SAIC Environmental Digital Services (EDS) Prototype
Available on: ![Chrome, Firefox, Internet Explorer, Safari, Android, iOS, Windows Mobile](http://saic-digital-services.cfapps.io/assets/img/readme/devices.png)

## 1. Introduction/Overview
SAIC developed a prototype to demonstrate digital services capabilities using agile scrum software development and API-driven architecture. This prototype integrates Census data and Greenhouse Gas emissions data consumed from publicly available web services creating a location-aware user experience to query, analyze, and map data. It supports EPA’s mission to protect public health and the environment aligning with one of EPA’s five strategic goals addressing climate change and improving air quality. 

SAIC’s prototype showcases a fast development approach accommodating user feedback, security, testing, scalable cloud infrastructure (Pivotal Cloud Foundry), seamless code movement using DevOps principles, and continuous deployment, ultimately reducing time to market. SAIC applied the scrum agile methodology to define the main features of the prototype, progressively elaborating on user stories throughout the sprint planning sessions. SAIC used a responsive design pattern to create a rich user experience irrespective of the viewing device.

## 2. Prototype
SAIC’s EDS prototype is available at this URL: http://saic-digital-services.cfapps.io/

## 3. Demonstration Video
A brief video showing an example of how to use the prototype is available at this location: <http://saic-digital-services.cfapps.io/assets/video/SAIC_EDS.swf> [![Swf](http://saic-digital-services.cfapps.io/assets/img/readme/File-SWF-icon.png)](<http://saic-digital-services.cfapps.io/assets/video/SAIC_EDS.swf>)

## 4. Architecture
The EDS prototype is available on all browsers (Chrome, Firefox, Internet Explorer, Safari) and mobile platforms (Android, iPhone, Windows Mobile). The prototype is based on a Single Page Application (SPA) design, which provides a highly responsive API-driven model to enable fast access to the data by loading it incrementally. This pattern is essential for low-bandwidth wireless networks and mobile devices. 
The prototype uses a MongoDB, Express JS, Angular JS, and Node JS (MEAN) stack. The MEAN stack is completely open source yet also provides the robustness and scale to create enterprise-grade applications that can be deployed to the cloud. The application also uses Leaflet, Chart.js, OpenStreetMap; a responsive design pattern using Bootstrap theme, HTML5, and CSS3; and an API-driven architecture (APIs are provided in Section 7).

Following is a brief description of what each technology is doing:

* MongoDB - The MongoDB database stores the GHG data for displaying the heat map results. 
* ExpressJS and NodeJS - The backend services calls to query MongoDB or the external web services are written in ExpressJS and NodeJS. 
* AngularJS, Bootstrap - The user interface of the application is written in AngularJS with Bootstrap for mobile browser support.
* Leaflet – Leaflet is a JavaScript library that coordinates overlaying the GHG heat map data over the OpenStreetMap information.
* Chart.js – This JavaScript-based open-source library is used to generate charts of the GHG data. The charts have a responsive design to work with different devices.
* OpenStreetMap – Open-source tool that provides the base mapping and visualization.

Figure 1 shows the application’s technical architecture.

![Figure 1. Technical Architecture](http://saic-digital-services.cfapps.io/assets/img/readme/work_arch.png)

**Figure 1. Technical Architecture**

### 4.1 Agile Process
SAIC used the scrum agile methodology to define the main features of the prototype and progressively elaborated on the user stories during the sprint planning session. The prototype also emphasizes the rapid development process whereby multiple developers are adding features to a Git-based code repository and yet the build process (using Gulp) and automated testing (using Jasmine) ensure that the product is built according to highest quality standards. 
The prototype is based on a stateless design and the use of a scalable cloud infrastructure (Pivotal Cloud Foundry) to support scalability. With the "Pay per use" cloud model, this design produces the most cost-effective solution. 
The prototype uses a Chef recipe to demonstrate the DevOps principle of seamless movement of code from developers to operations that provide the agility required to reduce the "time to market".
 
Figure 2 shows a screen capture of the JIRA scrum board that SAIC used to manage the process.
![Figure 2. JIRA Scrum Board](http://saic-digital-services.cfapps.io/assets/img/readme/EDS_Scrum_Board.png)

**Figure 2. JIRA Scrum Board**

### 4.2 Development and Testing 
SAIC is using Gulp to automate development and testing. We set up watches in the code to ensure that the JavaScript is passed through JSLint to find errors, run Jasmine tests, and minify and concatenate source files.
SAIC wrote Jasmine tests to verify the functionality and robustness of the application. We wrote tests for the Express service methods as well as Angular service and controller methods. These tests are automated through Gulp and Karma.
SAIC’s Jasmine tests can be accessed at this location: http://saic-digital-services.cfapps.io/testRunner.html

## 5. Installation 
SAIC used a DevOps approach to achieve faster development/deployment cycles. Within one sprint, we were able to complete many features quickly through writing a Chef recipe to automate the deployment whenever a new code commit is pushed to the source code repository.

Requirements and steps to install the database follow.

### 5.1 Requirements
The EDS prototype requires:

1. Node.js version 0.12.2 or later
2. NPM 2.7.4 or later
3. MongoDB 3.0.0 or later

### 5.2 Installation Steps
1. Download the source code from https://bitbucket.org/saicdigitalservices/digital-services.
2.	Configure the MongoDB instance.
	1. Note that the database export file that is used is located in the downloaded source code at db/db.json.
	2. Create a database to hold the facilities data for the heat map. 
	3. Configure a database user account with permission to add a collection to the database and import data to the collection. Giving the dbOwner role to the user account will allow both.
	4. Create a database user account that will have the read or readWrite role for the database. This account will be used by the application to access the facilities data.
	5. Using the MongoDB command line client, connect to the MongoDB server instance with the account created in step 2.3.
	6. In the command line client, type the following command:
		`use <database>`
	7. Create the facilities collection:
		`db.createCollection("facilities")`
	8. Go back to the command prompt and run the following command using the user account created in step 2.3:
		`mongoimport.exe -h <server>:<port> -d <database> -c "facilities" -u <user> -p <password> --file <db.json Filelocation>`
	9. The command line client will show that 7,288 documents were added.
3.	Configure the Node.js application
	1.	Update config.js
		1. Update the database portion of the configuration file
			1. Uri: Address of the MongoDB server
			2. Port: Port number that the MongoDB server uses
			3. Database: Name of the MongoDB database
			4. Username: User account created in step 2.4
			5. Password: Password for user account created in step 2.4
		2.	Update the web portion of the configuration file
			1. Port: The port the application uses. If an environmental variable is set for the port, it will use that; otherwise, it will use the second port.
			2. TokenSecret: If Authentication is used, this is the secret for encrypting the token.
	2.	Open a command prompt and navigate to the root folder of the source code.
	3.	To install all the Node modules, run:
		`npm install`
	4.	To install the Bower dependencies, run:
		`bower install`
4.	Start the Node.js server with the command:
	`node server.js`
5.	The server should start on the port that was specified in the config.js port setting.

## 6. Chef Recipe
SAIC is using Chef to automate development. Whenever a commit is pushed to the Bitbucket repository, a post-commit hook activates a Chef recipe on a remote server where the code is pulled from Bitbucket and tested against our test suite. If the tests pass, then the development configuration is replaced by the production configuration and the app is pushed immediately to Cloud Foundry. 
SAIC's Chef recipe is available in the source code repository, in the /chef folder.

## 7. API of Data Services
The prototype merges data from Census, GHG, and GIS (Mapping) to create a location-aware user experience to query and analyze data in new ways. The prototype uses the open source OpenStreetMap to render heat maps to visualize GHG data and enable rapid insight. The Angular app consumes two sets of JSON APIs, described below.

### 7.1. Greenhouse Gas API 
The Greenhouse Gas API from Facility-Level Information on GHG Tool (FLIGHT) (http://ghgdata.epa.gov) is used to supply emissions data for facilities across the United States, as well as per-sector information. This data is then merged with data obtained from the GIS APIs to produce the final result. All endpoints return JSON data.

* http://ghgdata.epa.gov/ghgp/service/populateSectorDashboard: Returns detailed emissions data per sector filtered by request parameters (filtering by county, for geolocation purposes). Results obtained from this endpoint are also used in the "top 5 facilities per sector" widget.
* http://ghgdata.epa.gov/ghgp/service/listFacility/US: Returns a list of facilities filtered by request parameters (filtering by county).

### 7.2 GIS APIs 
The following GIS APIs provide geocoding, county search, and demographic profile.

* http://geocode.arcgis.com/arcgis/rest/services/World/GeocodeServer: Esri public geocoding service is used for location search and reverse geocoding.
* http://map11.epa.gov/arcgis/rest/services/EMEF/Boundaries/MapServer/5: The EPA MyEnviroMapper map service is used to retrieve County information.
* http://ejscreen.epa.gov/arcgis/rest/services/ejscreen/census2012acs/MapServer/2: The EPA EJSCREEN demographic service is used to obtain demographic profile.

## 8. User Guide
The EDS prototype opens to a national view, as shown below. 
![Figure 3. EDS Prototype national view](http://saic-digital-services.cfapps.io/assets/img/readme/EDS_Full_Screen.jpg)

### 8.1 Search 
To find data for a county in the US:

1. Type location information in the Search field (e.g., city, county, state, ZIP code). 
2. Click the View button or press the Enter key.
The "Census Profile", "Heat Map of Direct Greenhouse Gas Emissions", and "Emission Totals by Industry Sector" panels will display data for that county.

### 8.2 Geolocate
To view data for the county where you are currently located, click the geolocate button. The browser might display a prompt whether to allow the browser to track your physical location. If allowed, the three panels ("Census Profile", "Heat Map of Direct Greenhouse Gas Emissions", and "Emission Totals by Industry Sector") will display data for your county.

### 8.3 Navigate the Heat Map

* Click the plus and minus buttons to zoom in and out.
* Double-click on the map to zoom in closer to that location.

![Figure 4. EDS Prototype heat map](http://saic-digital-services.cfapps.io/assets/img/readme/EDS_GHG_Heat_Map.jpg)

### 8.4 View Industry Sector
The "Emission Totals by Industry Sector" panel displays the breakdown of GHG emissions by industry sector in that county.

* Mouse over a colored segment in the pie chart to see the name of the sector and the total amount of GHG emitted by that sector (in million metric tons). 
* Clicking a colored segment in the pie chart will display a bar chart of the facilities that emit the most GHG for that industry sector, including the facility name and amount of emissions.

![Figure 5. EDS Prototype industry sector charts](http://saic-digital-services.cfapps.io/assets/img/readme/EDS_Sector_Panel.jpg)