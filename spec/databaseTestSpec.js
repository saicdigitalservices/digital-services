/**
 *
 * Created by alabdullahwi on 12/16/2015.
 */

var db = require('../app/services/mongoService');
require('jasmine-expect')


describe('mongo', function() {
    var result= undefined;

    beforeEach(function(done) {
       db.connect().then(function(_result) {
          result = _result;
          done();
       })
    });


    it('should be able to connect to the database', function(done) {
        expect(result.status).not.toEqual('error');
        expect(result.status).toEqual('success');
        done();
    })

    it('should be able to return a list of all facilities', function(done) {
        var length = -10;
        db.loadAll().then(function(results) {
            results.body.find().toArray(function (err, docs) {
                length = docs.length;
                expect(length).toEqual(7288);
                done();
            });
        });
    })

    it('should be able to return only heatmap data when needed', function(done) {
        db.loadHeatmapData().then(function(results) {
            results.body.toArray(function(err, docs){
                expect(docs.length).toEqual(7288);
                expect(docs[0].facilityId).toEqual(undefined);
                expect(docs[0].totalEmissions).not.toEqual(undefined);
                expect(docs[0].totalEmissions).toBeTruthy();
                expect(docs[0].coordinates).not.toEqual(undefined);
                expect(docs[0].coordinates.length).toEqual(2);
                expect(typeof(docs[0].coordinates)).toBe('object');
                expect(docs[0].coordinates[0]).toBeTruthy();
                expect(docs[0].coordinates[1]).toBeTruthy();
                done();
            });
        });

    });



});
