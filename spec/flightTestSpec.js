/**
 * Test the flight routes and services
 */
var request = require('superagent');
var config = require('../config').web;
var base = 'localhost' + ':' + config.port;

//needs server to be up
describe('flight service tests', function() {

    it('should get a json response when using the ping method', function(done) {
        request.get(base+"/flight/ping").end(function(err, response) {
           expect(response).not.toBe(null);
           expect(JSON.parse(response.text)).toBeTruthy();
           expect(JSON.parse(response.text).status).not.toEqual(undefined);
           done();
        });
    });
    
    it('should get sector data for the nation', function(done) {
        request.get(base+"/flight/getGHGSectors").end(function(err, response) {
           expect(response).not.toBe(null);
           expect(JSON.parse(response.text)).toBeTruthy();
           expect(JSON.parse(response.text).length).toBeGreaterThan(0);
           expect(JSON.parse(response.text)[0].value).toBeGreaterThan(0);
           done();
        });
    });
    
    it('should get sector data for a county', function(done) {
        request.get(base+"/flight/getGHGSectors?county=42011").end(function(err, response) {
           expect(response).not.toBe(null);
           expect(JSON.parse(response.text)).toBeTruthy();
           expect(JSON.parse(response.text).length).toBeGreaterThan(0);
           expect(JSON.parse(response.text)[5].value).toBeGreaterThan(0);
           done();
        });
    });
    
    it('should get the highest facilities data in a sector for the nation', function(done) {
        request.get(base+"/flight/getSectorTopFacilities?sector=Petroleum and Natural Gas Systems").end(function(err, response) {
           expect(response).not.toBe(null);
           expect(JSON.parse(response.text)).toBeTruthy();
           expect(JSON.parse(response.text).length).toBeGreaterThan(0);
           expect(JSON.parse(response.text)[0].totalNumber).toBeGreaterThan(0);
           done();
        });
    });
    
    it('should get the highest facilities data in a sector for a supplied sector', function(done) {
        request.get(base+"/flight/getSectorTopFacilities?sector=Power Plants&county=42011").end(function(err, response) {
           expect(response).not.toBe(null);
           expect(JSON.parse(response.text)).toBeTruthy();
           expect(JSON.parse(response.text).length).toBeGreaterThan(0);
           expect(JSON.parse(response.text)[0].total).toBeTruthy();
           done();
        });
    });
    
    it('should get the highest facilities data in a sector for a supplied sector', function(done) {
        request.get(base+"/flight/getSectorTopFacilities?sector=Power Plants&county=42011").end(function(err, response) {
           expect(response).not.toBe(null);
           expect(JSON.parse(response.text)).toBeTruthy();
           expect(JSON.parse(response.text).length).toBeGreaterThan(0);
           expect(JSON.parse(response.text)[0].total).toBeTruthy();
           done();
        });
    });
    
    it('should get the national census data', function(done) {
        request.get(base+"/flight/getNationalCensusData").end(function(err, response) {
           expect(response).not.toBe(null);
           expect(JSON.parse(response.text)).toBeTruthy();
           expect(JSON.parse(response.text).TOTALPOP).toBeTruthy();
           done();
        });
    });
    
    it('should get the census data for a county', function(done) {
        request.get(base+"/flight/getCensusData?county=34007").end(function(err, response) {
           expect(response).not.toBe(null);
           expect(JSON.parse(response.text)).toBeTruthy();
           expect(JSON.parse(response.text).fields.length).toBeGreaterThan(0);
           expect(JSON.parse(response.text).features[0].attributes.TOTALPOP).toBeTruthy();
           done();
        });
    });
});