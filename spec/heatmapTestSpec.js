/**
 * Created by alabdullahwi on 12/16/2015.
 */
var request = require('superagent');
var config = require('../config').web;
var base = 'localhost' + ':' + config.port;

//needs server to be up
describe('heatmap api tests', function() {

    it('should get a json response with coordinate and emission data when hitting heatmap url', function(done) {
        request.get(base+"/data/heatmap").end(function(err, response) {
           expect(response).not.toBe(null);
           expect(JSON.parse(response.text)).toBeTruthy();
           expect(JSON.parse(response.text).length).toEqual(7288);
           done();
        });
    });
});