#
# Cookbook Name:: digitalservices
# Recipe:: default
#
# Copyright 2015, SAIC
#
# All rights reserved - Do Not Redistribute
#

# pull code from bitbucket, ssh keys should be set up in ~/.ssh
target_location = "#{ENV['HOME']}/Deployments"
git "#{target_location}" do 
  repository "git@bitbucket.org:saicdigitalservices/digital-services.git"
  action :sync
end 


#replace config.json local dev db settings with prod db settings 
#data_bag_encryption_file = "#{ENV['HOME']}/chef-repo/data_bags/encrypted_data_bag_secret"  
mongo_config = data_bag_item('cloudfoundry','mongo_cloud_config') 
config_file =  "#{target_location}/config.js"  
ruby_block "configure cloud mongo" do 
 block do 
   sed = Chef::Util::FileEdit.new(config_file)
   sed.search_file_replace(/var\s*uri\s*=\s*["'].*['"]\s*;/, "var uri = '#{mongo_config["uri"]}'")
   sed.search_file_replace(/var\s*port\s*=\s*["'].*['"]\s*;/, "var port = '#{mongo_config["port"]}'")
   sed.search_file_replace(/var\s*database\s*=\s*['"].*['"]\s*;/, "var database = '#{mongo_config["database"]}'")
   sed.search_file_replace(/var\s*username\s*=\s*['"].*['"]\s*;/, "var username = '#{mongo_config["username"]}'")
   sed.search_file_replace(/var\s*password\s*=\s*['"].*['"]\s*;/, "var password = '#{mongo_config["password"]}'")
   sed.write_file
 end 
end 



#cloud foundry push 
cf_credentials = data_bag_item('cloudfoundry', 'login')   
bash "push to cloud foundry" do  
 cwd "#{target_location}" 
 code "cf login -a https://api.run.pivotal.io -u #{cf_credentials['email']} -p #{cf_credentials['password']} 
   cf push" 
end 







