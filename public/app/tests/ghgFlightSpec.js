/**
 * ghgFlightSpec holds the Jasimine tests for 
 */
describe("In the flightView module,", function () {

    beforeEach(angular.mock.module('flightView'));
    
    /**
     * Test the master controller
     */
    describe("the master controller", function() {
        beforeEach(angular.mock.module('masterController'));
        
        var ctrl;

        beforeEach(inject(function(_$controller_){
            // The injector unwraps the underscores (_) from around the parameter names when matching
            ctrl = _$controller_;
        }));
        
        var scope;
        var httpBackend;
        var authService;
        var locationService;
        
        /**
         * The test data
         */
        var successfulLoginData = {
			success: true,
			message: 'Enjoy your token!',
			token: "123471231"
			};
        var failedLoginData = {
           success: false,
           message: "Authentication failed"  
        };
        
       /**
        * Inject the required modules for the controller
        */
        beforeEach(inject(function ($rootScope, $controller, $httpBackend, $location, Auth) {
            scope = $rootScope.$new();
            httpBackend = $httpBackend;
            authService = Auth;
            locationService = $location; 

            ctrl = $controller("masterController", {$scope: scope, $location: locationService, Auth: authService});
        }));
        
        it('should parse a successful login', function() {
            ctrl.parseLogin(successfulLoginData);
            expect(ctrl.loggedIn).toEqual(true);
        });
        it('should parse a failed login', function() {
            ctrl.parseLogin(failedLoginData);
            expect(ctrl.loggedIn).toEqual(false);
        });
    });
    
    /**
     * Test the heat map controller
     */
    describe("the heat map controller", function() {
        beforeEach(angular.mock.module('heatMapController'));
        
        var ctrl;

        beforeEach(inject(function(_$controller_){
            // The injector unwraps the underscores (_) from around the parameter names when matching
            ctrl = _$controller_;
        }));
        
        var scope;
        var httpBackend;
        var flightService;
        var leafletData;
        var matchMedia;
        
        // sample heat map data
        var facilitiesData = [
        {
            "_id": "5670a1fbeda1c4fc54937b8f",
            "coordinates": [
            -96.53143,
            33.29814
            ],
            "totalEmissions": 241883.5
        }];
       
        beforeEach(inject(function ($rootScope, $controller, $httpBackend, Flight, _leafletData_, matchmedia) {
            scope = $rootScope.$new();
            httpBackend = $httpBackend;
            flightService = Flight;
            leafletData = _leafletData_;
            matchMedia = matchmedia;

            ctrl = $controller("heatMapController", {Flight: flightService, $scope: scope, leafletData: leafletData, matchmedia: matchMedia});
        }));
        
        it('should parse heat map facilities data', function() {
            var data = ctrl.parseHeatMapData(facilitiesData);
            expect(data[0][0]).toEqual(33.29814);
        });
    });

    /**
     * Test the search controller
     */
    describe("the search controller", function () {
        
        beforeEach(angular.mock.module('searchController'));
        
        var ctrl;

        beforeEach(inject(function(_$controller_){
            // The injector unwraps the underscores (_) from around the parameter names when matching
            ctrl = _$controller_;
        }));
        
        var scope;
        var httpBackend;
        var flightService;
        var geoLocation;
        
        // the sample geocode data for testing
        var currentGeocodeData = { coords: {latitude: 38.9218361, longitude: -77.23237519999999}};
        var geocodeServiceData = {"spatialReference":{"wkid":4326,"latestWkid":4326},"locations":[{"name":"Fairfax, Virginia, United States","extent":{"xmin":-77.333370000000002,"ymin":38.819220000000001,"xmax":-77.27937,"ymax":38.873220000000003},"feature":{"geometry":{"x":-77.306368558999679,"y":38.846219671000483},"attributes":{"Score":100,"Addr_Type":"POI"}}}]};
        var countyFIPSData = {"displayFieldName":"NAMELSAD","fieldAliases":{"STCN":"STCN","CNTY_NAME":"CNTY_NAME","STAB":"STAB"},"fields":[{"name":"STCN","type":"esriFieldTypeString","alias":"STCN","length":5},{"name":"CNTY_NAME","type":"esriFieldTypeString","alias":"CNTY_NAME","length":100},{"name":"STAB","type":"esriFieldTypeString","alias":"STAB","length":2}],"features":[{"attributes":{"STCN":"51013","CNTY_NAME":"Arlington County","STAB":"VA"}}]};
        var reverseGeocodeData = {"address":{"Address":"1710 SAIC Dr","Neighborhood":null,"City":"McLean","Subregion":null,"Region":"Virginia","Postal":"22102","PostalExt":null,"CountryCode":"USA","Match_addr":"1710 SAIC Dr, McLean, Virginia, 22102","Loc_name":"USA.PointAddress"},"location":{"x":-77.233051337921708,"y":38.922618673407555,"spatialReference":{"wkid":4326,"latestWkid":4326}}};
        var countyCensusData = {
        "displayFieldName": "CNTYNAME",
        "fieldAliases": {
            "TOTALPOP": "TOTALPOP",
            "HSHOLDS": "HSHOLDS",
            "PERC_MINOR": "PCT_MINORITY",
            "PERC_BELOW12": "PERC_BELOW12",
            "PERC_HS_DG": "PCT_EDU_HS_DG",
            "PERC_COL_DG": "PCT_EDU_COL_DG",
            "PERC_AGE_UNDER5": "PERC_AGE_UNDER5",
            "PERC_AGE_UNDER18": "PCT_AGE_LT18",
            "PERC_AGE_OVER64": "PERC_AGE_OVER64",
            "PERC_HOME_PRE50": "PCT_HOME_PRE50",
            "PERC_LAN_ENG_LTW": "PERC_LAN_ENG_LTW",
            "PERC_FEMALE": "PCT_FEMALES",
            "PERC_RENTERS": "PCT_RENTERS"
        },
        "fields": [
            {
            "name": "TOTALPOP",
            "type": "esriFieldTypeInteger",
            "alias": "TOTALPOP"
            },
            {
            "name": "HSHOLDS",
            "type": "esriFieldTypeInteger",
            "alias": "HSHOLDS"
            },
            {
            "name": "PERC_MINOR",
            "type": "esriFieldTypeInteger",
            "alias": "PCT_MINORITY"
            },
            {
            "name": "PERC_BELOW12",
            "type": "esriFieldTypeInteger",
            "alias": "PERC_BELOW12"
            },
            {
            "name": "PERC_HS_DG",
            "type": "esriFieldTypeInteger",
            "alias": "PCT_EDU_HS_DG"
            },
            {
            "name": "PERC_COL_DG",
            "type": "esriFieldTypeInteger",
            "alias": "PCT_EDU_COL_DG"
            },
            {
            "name": "PERC_AGE_UNDER5",
            "type": "esriFieldTypeInteger",
            "alias": "PERC_AGE_UNDER5"
            },
            {
            "name": "PERC_AGE_UNDER18",
            "type": "esriFieldTypeInteger",
            "alias": "PCT_AGE_LT18"
            },
            {
            "name": "PERC_AGE_OVER64",
            "type": "esriFieldTypeInteger",
            "alias": "PERC_AGE_OVER64"
            },
            {
            "name": "PERC_HOME_PRE50",
            "type": "esriFieldTypeInteger",
            "alias": "PCT_HOME_PRE50"
            },
            {
            "name": "PERC_LAN_ENG_LTW",
            "type": "esriFieldTypeInteger",
            "alias": "PERC_LAN_ENG_LTW"
            },
            {
            "name": "PERC_FEMALE",
            "type": "esriFieldTypeInteger",
            "alias": "PCT_FEMALES"
            },
            {
            "name": "PERC_RENTERS",
            "type": "esriFieldTypeInteger",
            "alias": "PCT_RENTERS"
            }
        ],
        "features": [
            {
            "attributes": {
                "TOTALPOP": 513660,
                "HSHOLDS": 188861,
                "PERC_MINOR": 40,
                "PERC_BELOW12": 13,
                "PERC_HS_DG": 32,
                "PERC_COL_DG": 29,
                "PERC_AGE_UNDER5": 6,
                "PERC_AGE_UNDER18": 24,
                "PERC_AGE_OVER64": 13,
                "PERC_HOME_PRE50": 28,
                "PERC_LAN_ENG_LTW": 4,
                "PERC_FEMALE": 52,
                "PERC_RENTERS": 31
            }
            }
        ]
        };
        
        var nationalCensusData = {
            "TOTALPOP": 309138711,
            "HSHOLDS": 115226802,
            "PERC_MINOR": 36.3,
            "PERC_BELOW12": 14.3,
            "PERC_HS_DG": 28.2,
            "PERC_COL_DG": 28.5,
            "PERC_AGE_UNDER5": 6.5,
            "PERC_AGE_UNDER18": 23.9,
            "PERC_AGE_OVER64": 13.2,
            "PERC_HOME_PRE50": 19.3,
            "PERC_LAN_ENG_LTW": 4.7,
            "PERC_FEMALE": 50.8,
            "PERC_RENTERS": 34.5
            };
        
        beforeEach(inject(function ($rootScope, $controller, $httpBackend, Flight, $q, geolocation) {
            scope = $rootScope.$new();
            httpBackend = $httpBackend;
            flightService = Flight;
            geoLocation = geolocation;

            ctrl = $controller("searchController", {$scope: scope, Flight: flightService, geolocation: geoLocation});
        }));

        it('should parse the current latitude', function() {
             ctrl.parseCurrentGeocode(currentGeocodeData);
             expect(ctrl.searchLatitude).toEqual(38.9218361);
        });
        
        it('should parse the current longitude', function() {
             ctrl.parseCurrentGeocode(currentGeocodeData);
             expect(ctrl.searchLongitude).toEqual(-77.23237519999999);
        });
        
        it('should parse the geocode service latitude', function() {
             ctrl.parseRetrivedGeocode(geocodeServiceData);
             expect(ctrl.searchLatitude).toEqual(38.846219671000483);
        });
        
        it('should parse the geocode service longitude', function() {
             ctrl.parseRetrivedGeocode(geocodeServiceData);
             expect(ctrl.searchLongitude).toEqual(-77.306368558999679);
        });
        
        it('should parse the county information retrieved from the county FIPS service', function() {
             ctrl.parseCountyName(countyFIPSData);
             expect(ctrl.selectedCountyFips).toEqual("51013");
        });
        
        it('should parse the county information retrieved from the reverse geocode service', function() {
             ctrl.parseCensusCountyName(reverseGeocodeData);
             expect(ctrl.censusCity).toEqual("McLean");
        });
        
        it('should parse the county census information retrieved from EJSCREEN census data service', function() {
             ctrl.parseCensusData(countyCensusData);
             expect(ctrl.censusGreater64Yr).toEqual("13%");
             expect(ctrl.censusHSDiploma).toEqual("32%");
             expect(ctrl.censusTotalPopulation).toEqual("513,660");
        });
        
        it('should parse the national census information', function() {
             ctrl.parseNationalCensusData(nationalCensusData);
             expect(ctrl.censusTotalPopulation).toEqual("309,138,711");
             expect(ctrl.censusBS).toEqual("29%");
             expect(ctrl.censusRenters).toEqual("35%");
        });
    });
    
    /**
     * Test the angular flight service
     */
    describe("the angular flight service ", function() {
        beforeEach(angular.mock.module('flightService'));
        
        var $httpBackend;
        var flightService;
        
        beforeEach(inject(function(_$httpBackend_, Flight) {
            $httpBackend = _$httpBackend_;
            flightService = Flight;
        }));
        
        // after each test, this ensure that every expected http calls have been realized and only them
        afterEach(function() {
            $httpBackend.verifyNoOutstandingExpectation();
            $httpBackend.verifyNoOutstandingRequest();
        });
        
        it('calls the geocode service', function() {
            var searchTerm = "McLean, VA";
            var returnData = {"spatialReference":{"wkid":4326,"latestWkid":4326},"locations":[{"name":"McLean, Virginia, United States","extent":{"xmin":-77.225477999999995,"ymin":38.886277,"xmax":-77.129478000000006,"ymax":38.982277000000003},"feature":{"geometry":{"x":-77.177477495999653,"y":38.934276857000498},"attributes":{"Score":100,"Addr_Type":"POI"}}}]};

            // write $http expectation and specify a mocked server response for the request
            // see https://docs.angularjs.org/api/ngMock/service/$httpBackend
            $httpBackend.expectGET("http://geocode.arcgis.com/arcgis/rest/services/World/GeocodeServer/find?text=" + searchTerm + "&maxLocations=6&outSR=4326&f=json").respond(200, returnData);

            var returnedData;
            flightService.getGeocode(searchTerm).success(function(result) {
                // check that returned result contains
                returnedData = result;
                expect(returnedData).toEqual(returnData);
            });
    
            // simulate server response
            $httpBackend.flush();
    
            // check that success handler has been called
            expect(returnedData).toBeDefined();
        });
        
        it('calls the reverse geocode service', function() {
            var longitude = -77.233051;
            var latitude = 38.922619;
            var returnData = {"address":{"Address":"1710 SAIC Dr","Neighborhood":null,"City":"McLean","Subregion":null,"Region":"Virginia","Postal":"22102","PostalExt":null,"CountryCode":"USA","Match_addr":"1710 SAIC Dr, McLean, Virginia, 22102","Loc_name":"USA.PointAddress"},"location":{"x":-77.233051337921708,"y":38.922618673407555,"spatialReference":{"wkid":4326,"latestWkid":4326}}};

            // write $http expectation and specify a mocked server response for the request
            // see https://docs.angularjs.org/api/ngMock/service/$httpBackend
            $httpBackend.expectGET("http://geocode.arcgis.com/arcgis/rest/services/World/GeocodeServer/reverseGeocode?location=" + longitude + "," + latitude + "&outSR=&f=json").respond(200, returnData);

            var returnedData;
            flightService.getReverseGeocode(latitude, longitude).success(function(result) {
                // check that returned result contains
                returnedData = result;
                expect(returnedData).toEqual(returnData);
            });
    
            // simulate server response
            $httpBackend.flush();
    
            // check that success handler has been called
            expect(returnedData).toBeDefined();
        });
        
        it('calls get county FIPS service', function() {
            var longitude = -77.233051;
            var latitude = 38.922619;
            var returnData = {"displayFieldName":"NAMELSAD","fieldAliases":{"STCN":"STCN","CNTY_NAME":"CNTY_NAME","STAB":"STAB"},"fields":[{"name":"STCN","type":"esriFieldTypeString","alias":"STCN","length":5},{"name":"CNTY_NAME","type":"esriFieldTypeString","alias":"CNTY_NAME","length":100},{"name":"STAB","type":"esriFieldTypeString","alias":"STAB","length":2}],"features":[{"attributes":{"STCN":"51059","CNTY_NAME":"Fairfax County","STAB":"VA"}}]};

            // write $http expectation and specify a mocked server response for the request
            // see https://docs.angularjs.org/api/ngMock/service/$httpBackend
            $httpBackend.expectGET("https://map11.epa.gov/arcgis/rest/services/EMEF/Boundaries/MapServer/5/query?f=json&returnGeometry=false&spatialRel=esriSpatialRelIntersects&maxAllowableOffset=19&geometry=" + longitude + "," + latitude + "&geometryType=esriGeometryPoint&inSR=4326&outFields=STCN,CNTY_NAME,STAB").respond(200, returnData);

            var returnedData;
            flightService.getCountyFIPS(latitude, longitude).success(function(result) {
                // check that returned result contains
                returnedData = result;
                expect(returnedData).toEqual(returnData);
            });
    
            // simulate server response
            $httpBackend.flush();
    
            // check that success handler has been called
            expect(returnedData).toBeDefined();
        });
        
        it('calls get county census data service', function() {
            var countyFIPS = "34007";
            var returnData = {
  "displayFieldName": "CNTYNAME",
  "fieldAliases": {
    "TOTALPOP": "TOTALPOP",
    "HSHOLDS": "HSHOLDS",
    "PERC_MINOR": "PCT_MINORITY",
    "PERC_BELOW12": "PERC_BELOW12",
    "PERC_HS_DG": "PCT_EDU_HS_DG",
    "PERC_COL_DG": "PCT_EDU_COL_DG",
    "PERC_AGE_UNDER5": "PERC_AGE_UNDER5",
    "PERC_AGE_UNDER18": "PCT_AGE_LT18",
    "PERC_AGE_OVER64": "PERC_AGE_OVER64",
    "PERC_HOME_PRE50": "PCT_HOME_PRE50",
    "PERC_LAN_ENG_LTW": "PERC_LAN_ENG_LTW",
    "PERC_FEMALE": "PCT_FEMALES",
    "PERC_RENTERS": "PCT_RENTERS"
  },
  "fields": [
    {
      "name": "TOTALPOP",
      "type": "esriFieldTypeInteger",
      "alias": "TOTALPOP"
    },
    {
      "name": "HSHOLDS",
      "type": "esriFieldTypeInteger",
      "alias": "HSHOLDS"
    },
    {
      "name": "PERC_MINOR",
      "type": "esriFieldTypeInteger",
      "alias": "PCT_MINORITY"
    },
    {
      "name": "PERC_BELOW12",
      "type": "esriFieldTypeInteger",
      "alias": "PERC_BELOW12"
    },
    {
      "name": "PERC_HS_DG",
      "type": "esriFieldTypeInteger",
      "alias": "PCT_EDU_HS_DG"
    },
    {
      "name": "PERC_COL_DG",
      "type": "esriFieldTypeInteger",
      "alias": "PCT_EDU_COL_DG"
    },
    {
      "name": "PERC_AGE_UNDER5",
      "type": "esriFieldTypeInteger",
      "alias": "PERC_AGE_UNDER5"
    },
    {
      "name": "PERC_AGE_UNDER18",
      "type": "esriFieldTypeInteger",
      "alias": "PCT_AGE_LT18"
    },
    {
      "name": "PERC_AGE_OVER64",
      "type": "esriFieldTypeInteger",
      "alias": "PERC_AGE_OVER64"
    },
    {
      "name": "PERC_HOME_PRE50",
      "type": "esriFieldTypeInteger",
      "alias": "PCT_HOME_PRE50"
    },
    {
      "name": "PERC_LAN_ENG_LTW",
      "type": "esriFieldTypeInteger",
      "alias": "PERC_LAN_ENG_LTW"
    },
    {
      "name": "PERC_FEMALE",
      "type": "esriFieldTypeInteger",
      "alias": "PCT_FEMALES"
    },
    {
      "name": "PERC_RENTERS",
      "type": "esriFieldTypeInteger",
      "alias": "PCT_RENTERS"
    }
  ],
  "features": [
    {
      "attributes": {
        "TOTALPOP": 513660,
        "HSHOLDS": 188861,
        "PERC_MINOR": 40,
        "PERC_BELOW12": 13,
        "PERC_HS_DG": 32,
        "PERC_COL_DG": 29,
        "PERC_AGE_UNDER5": 6,
        "PERC_AGE_UNDER18": 24,
        "PERC_AGE_OVER64": 13,
        "PERC_HOME_PRE50": 28,
        "PERC_LAN_ENG_LTW": 4,
        "PERC_FEMALE": 52,
        "PERC_RENTERS": 31
      }
    }
  ]
};

            // write $http expectation and specify a mocked server response for the request
            // see https://docs.angularjs.org/api/ngMock/service/$httpBackend
            $httpBackend.expectGET("/flight/getCensusData?county=" + countyFIPS).respond(200, returnData);

            var returnedData;
            flightService.getCensusData(countyFIPS).success(function(result) {
                // check that returned result contains
                returnedData = result;
                expect(returnedData).toEqual(returnData);
            });
    
            // simulate server response
            $httpBackend.flush();
    
            // check that success handler has been called
            expect(returnedData).toBeDefined();
        });
        
        it('calls get national census data service', function() {
            var returnData = {
                "TOTALPOP": 309138711,
                "HSHOLDS": 115226802,
                "PERC_MINOR": 36.3,
                "PERC_BELOW12": 14.3,
                "PERC_HS_DG": 28.2,
                "PERC_COL_DG": 28.5,
                "PERC_AGE_UNDER5": 6.5,
                "PERC_AGE_UNDER18": 23.9,
                "PERC_AGE_OVER64": 13.2,
                "PERC_HOME_PRE50": 19.3,
                "PERC_LAN_ENG_LTW": 4.7,
                "PERC_FEMALE": 50.8,
                "PERC_RENTERS": 34.5
                };

            // write $http expectation and specify a mocked server response for the request
            // see https://docs.angularjs.org/api/ngMock/service/$httpBackend
            $httpBackend.expectGET("/flight/getNationalCensusData").respond(200, returnData);

            var returnedData;
            flightService.getNationalCensusData().success(function(result) {
                // check that returned result contains
                returnedData = result;
                expect(returnedData).toEqual(returnData);
            });
    
            // simulate server response
            $httpBackend.flush();
    
            // check that success handler has been called
            expect(returnedData).toBeDefined();
        });
        
        it('calls get heat map data service', function() {
            var returnData = [{"_id":"5670a1fbeda1c4fc54937b8f","coordinates":[-96.53143,33.29814],"totalEmissions":241883.5},{"_id":"5670a1fbeda1c4fc54937b90","coordinates":[-83.1560461,37.2190994],"totalEmissions":225708.25},{"_id":"5670a1fbeda1c4fc54937b91","coordinates":[-83.18126,37.236617],"totalEmissions":68761},{"_id":"5670a1fbeda1c4fc54937b92","coordinates":[-74,40.663],"totalEmissions":40722.2},{"_id":"5670a1fbeda1c4fc54937b93","coordinates":[-107.4455,36.7452],"totalEmissions":15353.192}];

            // write $http expectation and specify a mocked server response for the request
            // see https://docs.angularjs.org/api/ngMock/service/$httpBackend
            $httpBackend.expectGET("/data/heatmap").respond(200, returnData);

            var returnedData;
            flightService.getHeatmapData().success(function(result) {
                // check that returned result contains
                returnedData = result;
                expect(returnedData.length).toBeGreaterThan(0);
            });
    
            // simulate server response
            $httpBackend.flush();
    
            // check that success handler has been called
            expect(returnedData).toBeDefined();
        });
        
        it('calls get national sectors emission totals service', function() {
            var returnData = [
            {
                "name": "Pulp and Paper",
                "value": 39
            },
            {
                "name": "Metals",
                "value": 103
            },
            {
                "name": "Waste",
                "value": 113
            },
            {
                "name": "Minerals",
                "value": 117
            },
            {
                "name": "Other",
                "value": 142
            },
            {
                "name": "Refineries",
                "value": 175
            },
            {
                "name": "Chemicals",
                "value": 177
            },
            {
                "name": "Petroleum and Natural Gas Systems",
                "value": 236
            },
            {
                "name": "Power Plants",
                "value": 2101
            }
            ];

            // write $http expectation and specify a mocked server response for the request
            // see https://docs.angularjs.org/api/ngMock/service/$httpBackend
            $httpBackend.expectGET("/flight/getGHGSectors").respond(200, returnData);

            var returnedData;
            flightService.getNationalSectorData().success(function(result) {
                // check that returned result contains
                returnedData = result;
                expect(returnedData).toEqual(returnData);
            });
    
            // simulate server response
            $httpBackend.flush();
    
            // check that success handler has been called
            expect(returnedData).toBeDefined();
        });
        
        it('calls get county sectors emission totals service', function() {
            var countyFIPS = "06067";
            var returnData =[
            {
                "name": "Metals",
                "value": 0
            },
            {
                "name": "Minerals",
                "value": 0
            },
            {
                "name": "Refineries",
                "value": 0
            },
            {
                "name": "Pulp and Paper",
                "value": 0
            },
            {
                "name": "Petroleum and Natural Gas Systems",
                "value": 0
            },
            {
                "name": "Chemicals",
                "value": 44826
            },
            {
                "name": "Other",
                "value": 61736
            },
            {
                "name": "Waste",
                "value": 215902
            },
            {
                "name": "Power Plants",
                "value": 2393744
            }
            ];

            // write $http expectation and specify a mocked server response for the request
            // see https://docs.angularjs.org/api/ngMock/service/$httpBackend
            $httpBackend.expectGET("/flight/getGHGSectors?county=" + countyFIPS).respond(200, returnData);

            var returnedData;
            flightService.getCountySectorData(countyFIPS).success(function(result) {
                // check that returned result contains
                returnedData = result;
                expect(returnedData).toEqual(returnData);
            });
    
            // simulate server response
            $httpBackend.flush();
    
            // check that success handler has been called
            expect(returnedData).toBeDefined();
        });
        
        it('calls get national highest facilities in a sector service', function() {
            var sector = "Petroleum and Natural Gas Systems";
            var returnData = [
            {
                "id": "id617",
                "facility": "ConocoPhillips San Juan Basin (580)",
                "city": "Farmington",
                "state": "NM",
                "total": "3,440,450",
                "sectors": "Petroleum and Natural Gas Systems",
                "totalNumber": 3440450
            },
            {
                "id": "id1968",
                "facility": "Shute Creek Facility",
                "city": "KEMMERER",
                "state": "WY",
                "total": "3,034,525",
                "sectors": "Petroleum and Natural Gas Systems",
                "totalNumber": 3034525
            },
            {
                "id": "id241",
                "facility": "BPXA CENTRAL COMPRESSOR PLANT",
                "city": "Prudhoe Bay",
                "state": "AK",
                "total": "2,881,196",
                "sectors": "Petroleum and Natural Gas Systems",
                "totalNumber": 2881196
            },
            {
                "id": "id521",
                "facility": "Chesapeake Operating Inc., 360 - Anadarko Basin",
                "city": "Oklahoma City",
                "state": "OK",
                "total": "2,218,586",
                "sectors": "Petroleum and Natural Gas Systems",
                "totalNumber": 2218586
            },
            {
                "id": "id2415",
                "facility": "Whiting Oil and Gas Corporation 395 Williston Basin",
                "city": "Denver",
                "state": "CO",
                "total": "1,929,966",
                "sectors": "Petroleum and Natural Gas Systems",
                "totalNumber": 1929966
            }
            ];
            
            // write $http expectation and specify a mocked server response for the request
            // see https://docs.angularjs.org/api/ngMock/service/$httpBackend
            $httpBackend.expectGET("/flight/getSectorTopFacilities?sector=" + sector).respond(200, returnData);

            var returnedData;
            flightService.getNationalSectorTopFacilitiesData(sector).success(function(result) {
                // check that returned result contains
                returnedData = result;
                expect(returnedData).toEqual(returnData);
            });
    
            // simulate server response
            $httpBackend.flush();
    
            // check that success handler has been called
            expect(returnedData).toBeDefined();
        });
        
        it('calls get county highest facilities in a sector service', function() {
            var sector = "Power Plants";
            var countyFIPS = "42011";
            var returnData = [
                {
                    "id": "id0",
                    "facility": "Ontelaunee Energy Center",
                    "city": "READING",
                    "state": "PA",
                    "total": "1,368,282",
                    "sectors": "Power Plants",
                    "totalNumber": 1368282
                },
                {
                    "id": "id1",
                    "facility": "Titus",
                    "city": "BIRDSBORO",
                    "state": "PA",
                    "total": "1,477",
                    "sectors": "Power Plants",
                    "totalNumber": 1477
                }
                ];
            
            // write $http expectation and specify a mocked server response for the request
            // see https://docs.angularjs.org/api/ngMock/service/$httpBackend
            $httpBackend.expectGET("/flight/getSectorTopFacilities?county=" + countyFIPS + "&sector=" + sector).respond(200, returnData);

            var returnedData;
            flightService.getCountySectorTopFacilitiesData(countyFIPS, sector).success(function(result) {
                // check that returned result contains
                returnedData = result;
                expect(returnedData).toEqual(returnData);
            });
    
            // simulate server response
            $httpBackend.flush();
    
            // check that success handler has been called
            expect(returnedData).toBeDefined();
        });
    });
    
     /**
     * Test the angular authentication service
     */
    describe("the angular authentication service ", function() {
        beforeEach(angular.mock.module('authService'));
        
        var $httpBackend;
        var authService;
        var token;
        var deferred;
        
        beforeEach(inject(function(Auth, _$httpBackend_, AuthToken, _$q_) {
            $httpBackend = _$httpBackend_;
            token = AuthToken;
            deferred = _$q_;
            authService = Auth;
        }));
        
        // after each test, this ensure that every expected http calls have been realized and only them
        afterEach(function() {
            $httpBackend.verifyNoOutstandingExpectation();
            $httpBackend.verifyNoOutstandingRequest();
        });
        
        it('calls the authentication service and successfully logs in', function() {
            var username = "jim";
            var password = "P@ssw0rd";
            var returnData = {
            "success": true,
            "message": "Enjoy your token!",
            "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJuYW1lIjoiYXBwcm92ZWRVc2VyIiwiaWF0IjoxNDUwODI2ODk3LCJleHAiOjE0NTA5MTMyOTd9.q5WuSKxE8nAJGdMaEszkKGlgoRuHD9ImDQMYEEYN9gY"
            };

            // write $http expectation and specify a mocked server response for the request
            // see https://docs.angularjs.org/api/ngMock/service/$httpBackend
            $httpBackend.expectPOST("/flight/authenticate", {username: username, password: password}).respond(200, returnData);

            var returnedData;
            authService.login(username, password).success(function(result) {
                // check that returned result contains
                returnedData = result;
                expect(returnedData.success).toEqual(true);
            });
    
            // simulate server response
            $httpBackend.flush();
    
            // check that success handler has been called
            expect(returnedData).toBeDefined();
        });
    });
});