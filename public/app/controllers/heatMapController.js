/*****************************************
 * heatMapController manipulates the leaflet directive for the Open Street Maps. 
 * It also loads the data and creates an image overlay for the heat map data from Greenhouse Gas.
 * leaflet-directive is used to manipulate the leaflet plugin
 * matchmedia-ng is used to detect the user's device
 * darthwade.loading is used to display a loading animation while data is being queried
 *****************************************/
angular.module('heatMapController', ["leaflet-directive", "matchmedia-ng", "darthwade.loading"])
// uses the flight service, Leaflet, MatchMedia (to detect devices)
.controller('heatMapController', ["Flight", "$scope", "leafletData", "matchmedia", "$loading", function(Flight, $scope, leafletData, matchmedia, $loading) {
    var vm = this;

    /*
    * Do a check to see if the current device is a phone, if it is, the initial zoom is set to be a bit closer into the selected location
    */
    var isMobile = matchmedia.isPhone();
    var initialZoom = 4;
    if(isMobile === true) {
        initialZoom = 6;
    }

    /*
    * Extends the angular scope to set the properties for the leaflet directive using OpenStreetMaps
    */
    angular.extend($scope, {
        center: {
            lat: 40.307,
            lng: -95.312,
            zoom: initialZoom
        },
        layers: {
            baselayers: {
                osm: {
                    name: 'OpenStreetMap',
                    url: 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
                    type: 'xyz',
                    layerParams: {
                        showOnSelector: false,
                        noWrap: true,
                        attribution: 'Map data &copy; <a href="http://openstreetmap.org" target="_blank">OpenStreetMap</a>, Imagery © <a href="http://cloudmade.com" target="_blank">CloudMade</a>'
                    },
                    maxZoom: 18
                }
            }
        }
    });

    /**
     * Get the facilities heat map data in a REST call to the backend service.
     * The data is then parsed and set as an image overlay on the leaflet map.
     */
    vm.requestFacilities = function() {
        $loading.start("heatmap");
        Flight.getHeatmapData()
            // promise
            .success(function(data) {
                var dataArray = vm.parseHeatMapData(data);
                
                var heat = L.heatLayer(dataArray);
                var opt = {
                    radius: 20,
                    blur: 10
                };
                heat.setOptions(opt);
                leafletData.getMap().then(function(map) {
                    heat.addTo(map);
                });
            }).
            finally(function(data) {
                $loading.finish("heatmap");
            });
    };
    
    /**
     * Parse the heat map data that is returned from the backend service
     */
    vm.parseHeatMapData = function(data) {
        var dataarray = [];
        for (var i=0; i< data.length; i++) {
            var att = data[i];
            var n = att["totalEmissions"];
            var point = [att["coordinates"][1],att["coordinates"][0],n/10000];
            dataarray.push(point);
        }
        
        return dataarray;
    };

    // request the facility data upon startup of the heat map controller
    vm.requestFacilities();
}]);
