/*****************************************
 * masterController is the parent controller for all of the views in the application. 
 * It is used to control logging in and out of the app if enabled and also could be used to control the display of navigation elements (if in a mobile or desktop display).
 * authService is used to provide basic authentication 
 *****************************************/
angular.module("masterController", ["authService"])

.controller('masterController', ["$rootScope", "$location", "Auth", function($rootScope, $location, Auth) {
	var vm = this;
	
	// check the Angular authentication service to see if the current user is already logged in
	vm.loggedIn = true; //Auth.isLoggedIn();
	
	/*
	* Every time a view is loaded, the authentication service is checked to make sure that the user is logged in
	*/
	$rootScope.$on("$routeChangeStart", function() {
		vm.loggedIn = true; Auth.isLoggedIn();
	});
	
	/*
	* Perform a login for the user
	*/
	vm.doLogin = function() {
		vm.processing = true;

		// clear the error
		vm.error = "";
		
		// call the Auth service login() function
		Auth.login(vm.loginData.username, vm.loginData.password)
			.success(function(data) {
				vm.processing = false;
				if(vm.parseLogin(data)) {
					// if a user successfully logs in, redirect to homepage page
					$location.path('/');
				}
			});
	};
	
	/*
	* Parse the login result and set if a user is logged in
	*/
	vm.parseLogin = function(data) {
		if(data.success === true) {
			vm.loggedIn = true;
			return true;
		}
		else {
			vm.loggedIn = false;
			return false;
		}
	};
	
	/*
	* Logout the user and remove the token, redirecting back to the homepage
	*/
	vm.doLogout = function() {
		Auth.logout();
		vm.loggedIn = false;
		$location.path("/");	
	};
}]);