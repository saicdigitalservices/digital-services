/*****************************************
 * heatMapController manipulates the leaflet directive for the Open Street Maps. 
 * It also loads the data and creates an image overlay for the heat map data from Greenhouse Gas.
 * leaflet-directive is used to manipulate the leaflet plugin
 * matchmedia-ng is used to detect the user's device
 * darthwade.loading is used to display a loading animation while data is being queried
 *****************************************/
angular.module('heatMapController', ["leaflet-directive", "matchmedia-ng", "darthwade.loading"])
// uses the flight service, Leaflet, MatchMedia (to detect devices)
.controller('heatMapController', ["Flight", "$scope", "leafletData", "matchmedia", "$loading", function(Flight, $scope, leafletData, matchmedia, $loading) {
    var vm = this;

    /*
    * Do a check to see if the current device is a phone, if it is, the initial zoom is set to be a bit closer into the selected location
    */
    var isMobile = matchmedia.isPhone();
    var initialZoom = 4;
    if(isMobile === true) {
        initialZoom = 6;
    }

    /*
    * Extends the angular scope to set the properties for the leaflet directive using OpenStreetMaps
    */
    angular.extend($scope, {
        center: {
            lat: 40.307,
            lng: -95.312,
            zoom: initialZoom
        },
        layers: {
            baselayers: {
                osm: {
                    name: 'OpenStreetMap',
                    url: 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
                    type: 'xyz',
                    layerParams: {
                        showOnSelector: false,
                        noWrap: true,
                        attribution: 'Map data &copy; <a href="http://openstreetmap.org" target="_blank">OpenStreetMap</a>, Imagery © <a href="http://cloudmade.com" target="_blank">CloudMade</a>'
                    },
                    maxZoom: 18
                }
            }
        }
    });

    /**
     * Get the facilities heat map data in a REST call to the backend service.
     * The data is then parsed and set as an image overlay on the leaflet map.
     */
    vm.requestFacilities = function() {
        $loading.start("heatmap");
        Flight.getHeatmapData()
            // promise
            .success(function(data) {
                var dataArray = vm.parseHeatMapData(data);
                
                var heat = L.heatLayer(dataArray);
                var opt = {
                    radius: 20,
                    blur: 10
                };
                heat.setOptions(opt);
                leafletData.getMap().then(function(map) {
                    heat.addTo(map);
                });
            }).
            finally(function(data) {
                $loading.finish("heatmap");
            });
    };
    
    /**
     * Parse the heat map data that is returned from the backend service
     */
    vm.parseHeatMapData = function(data) {
        var dataarray = [];
        for (var i=0; i< data.length; i++) {
            var att = data[i];
            var n = att["totalEmissions"];
            var point = [att["coordinates"][1],att["coordinates"][0],n/10000];
            dataarray.push(point);
        }
        
        return dataarray;
    };

    // request the facility data upon startup of the heat map controller
    vm.requestFacilities();
}]);

/*****************************************
 * masterController is the parent controller for all of the views in the application. 
 * It is used to control logging in and out of the app if enabled and also could be used to control the display of navigation elements (if in a mobile or desktop display).
 * authService is used to provide basic authentication 
 *****************************************/
angular.module("masterController", ["authService"])

.controller('masterController', ["$rootScope", "$location", "Auth", function($rootScope, $location, Auth) {
	var vm = this;
	
	// check the Angular authentication service to see if the current user is already logged in
	vm.loggedIn = true; //Auth.isLoggedIn();
	
	/*
	* Every time a view is loaded, the authentication service is checked to make sure that the user is logged in
	*/
	$rootScope.$on("$routeChangeStart", function() {
		vm.loggedIn = true; Auth.isLoggedIn();
	});
	
	/*
	* Perform a login for the user
	*/
	vm.doLogin = function() {
		vm.processing = true;

		// clear the error
		vm.error = "";
		
		// call the Auth service login() function
		Auth.login(vm.loginData.username, vm.loginData.password)
			.success(function(data) {
				vm.processing = false;
				if(vm.parseLogin(data)) {
					// if a user successfully logs in, redirect to homepage page
					$location.path('/');
				}
			});
	};
	
	/*
	* Parse the login result and set if a user is logged in
	*/
	vm.parseLogin = function(data) {
		if(data.success === true) {
			vm.loggedIn = true;
			return true;
		}
		else {
			vm.loggedIn = false;
			return false;
		}
	};
	
	/*
	* Logout the user and remove the token, redirecting back to the homepage
	*/
	vm.doLogout = function() {
		Auth.logout();
		vm.loggedIn = false;
		$location.path("/");	
	};
}]);
/**
 * searchController is the main "guts" of the application.
 * It calls the backend services to get the Greenhouse Gas data, census information, geocode information, and also updates the heat map to be centered on the proper location.
 * flightService is used to connect to the various services that serve data for the application
 * chart.js is used to create the doughnut and bar charts
 * geolocation is used to get the current latitude and longitude of the user
 * ngRoute is used to get a search query from the current url
 * darthwade.loading is used to display a loading animation while data is being queried
 */

angular.module("searchController", ["flightService", "chart.js", "geolocation", "ngRoute", "darthwade.loading"])

// home controller
.controller('searchController', ["Flight", "geolocation", "$routeParams", "$location", "$timeout", "$scope", "$loading", function(Flight, geolocation, $routeParams, $location, $timeout, $scope, $loading) {
	var vm = this;
  
  // set labels
  var sectorChartTitle = "Sectors";
  var sectorChartFooter = "Million Metric Tons";
  var facilityChartFooter = "Metric Tons";
  
  // get the initial 
  var initialSearch = $routeParams.q;
  var initialLoad = true;
  
  // search validation
  vm.validSearch = true;
  vm.noResultsFound = false;
  
  // values for turning on and off the appropriate labels and 
  vm.isNational = true;
  vm.selectedCountyFips = null;
  vm.selectedCountyName = null;
  vm.selectedCountyState = null;
  vm.viewingFacilities = false;
  vm.selectedSector = "";
  vm.noSectorDataFound = false;
  vm.sectorUnits = "Million Metric Units";
  vm.facilityUnits = "Metric Tons";
  
  // the current latitude and longitude from the search
  vm.searchLatitude = {};
  vm.searchLongitude = {};
  
  // labels and data for the sector chart
  vm.sectorChartLabels = [];
  vm.sectorChartData = [];
  
  // labels, series, and data for the top facilities chart
  vm.facilityChartData = [];
  vm.facilityChartLabels = [];
  vm.facilityChartSeries = ["2014"];
  
  // the colors for the sectors chart
  vm.sectorColors = [{
					fillColor: '#4573a7',
					strokeColor: '#4573a7',
					pointColor: '#4573a7',
					pointStrokeColor: '#4573a7',
					pointHighlightFill: '#4573a7',
					pointHighlightStroke: '#4573a7'
				},
				{
					fillColor: '#aa4644',
					strokeColor: '#aa4644',
					pointColor: '#aa4644',
					pointStrokeColor: '#aa4644',
					pointHighlightFill: '#aa4644',
					pointHighlightStroke: '#aa4644'
				},
				{
					fillColor: '#89a54e',
					strokeColor: '#89a54e',
					pointColor: '#89a54e',
					pointStrokeColor: '#89a54e',
					pointHighlightFill: '#89a54e',
					pointHighlightStroke: '#89a54e'
				},
				{
					fillColor: '#71588f',
					strokeColor: '#71588f',
					pointColor: '#71588f',
					pointStrokeColor: '#71588f',
					pointHighlightFill: '#71588f',
					pointHighlightStroke: '#71588f'
				},
				{
					fillColor: '#4298af',
					strokeColor: '#4298af',
					pointColor: '#4298af',
					pointStrokeColor: '#4298af',
					pointHighlightFill: '#4298af',
					pointHighlightStroke: '#4298af'
				},
				{
					fillColor: '#db843d',
					strokeColor: '#db843d',
					pointColor: '#db843d',
					pointStrokeColor: '#db843d',
					pointHighlightFill: '#db843d',
					pointHighlightStroke: '#db843d'
				},
				{
					fillColor: '#93a9d0',
					strokeColor: '#93a9d0',
					pointColor: '#93a9d0',
					pointStrokeColor: '#93a9d0',
					pointHighlightFill: '#93a9d0',
					pointHighlightStroke: '#93a9d0'
				},
				{
					fillColor: '#d09392',
					strokeColor: '#d09392',
					pointColor: '#d09392',
					pointStrokeColor: '#d09392',
					pointHighlightFill: '#d09392',
					pointHighlightStroke: '#d09392'
				},
				{
					fillColor: '#bacd96',
					strokeColor: '#bacd96',
					pointColor: '#bacd96',
					pointStrokeColor: '#bacd96',
					pointHighlightFill: '#bacd96',
					pointHighlightStroke: '#bacd96'
				},
			];
  
  
  /**
   * After the user does a search, call the geocode service to get the latitude and longitude and then display the county data
   */
  vm.getGeocode = function() {
    // if the uri contains a search query, that is used
    $location.search({q: vm.searchText});
    
    var validSearch = true;
    
    // if a search term was not supplied, flag it and display an error message
    if(vm.searchText === undefined || vm.searchText.length === 0) {
      validSearch = false;
    }
    
    // if a valid search was entered, get the geocode information for the search and then display the county information
    if(validSearch) {
      Flight.getGeocode(vm.searchText)
        .success(function(data) {
          vm.parseRetrivedGeocode(data);
          if(!vm.noResultsFound) {
            // turn off the national data from being displayed
            vm.isNational = false;
            vm.viewingFacilities = false;
            vm.displayCountyData();
          }
        });
    }
    else {
      // get the national data
      vm.displayNationalSectorData();
      vm.displayNationalCensusData();
    }
  };
  
  /**
   * Parse the value from the geocode service call to get the latitude and longitude 
   */
  vm.parseRetrivedGeocode = function(data) {
    if(data !== null && data.locations.length > 0) {
        vm.searchLatitude = data.locations[0].feature.geometry.y;
        vm.searchLongitude = data.locations[0].feature.geometry.x;
        vm.noResultsFound = false;
      }
      else {
          vm.noResultsFound = true;
      }
  };
  
  /**
   * If the user selects to use the browser geolocation feature this method will get the latitude and longitude and then display the county information
   */
  vm.getCurrentGeocode = function() {
    geolocation.getLocation().then(function(data){
      vm.parseCurrentGeocode(data);
      // turn off the national data from being displayed
      vm.isNational = false;
      vm.viewingFacilities = false;
      vm.displayCountyData();
    });
  };
  
   /**
   * Parse the value from the geolcation call to get the latitude and longitude 
   */
  vm.parseCurrentGeocode = function(data) {
      vm.searchLatitude = data.coords.latitude;
      vm.searchLongitude = data.coords.longitude;
  };
  
  /**
   * Display the county sector data
   */
  vm.displayCountyData = function() {
    vm.updateHeatMap();
    Flight.getCountyFIPS(vm.searchLatitude, vm.searchLongitude)
    .success(function(data) {
        if(data !== undefined && data.features !== undefined && data.features.length > 0 && data.features[0].attributes !== undefined) {
        vm.noResultsFound = false;
        vm.parseCountyName(data);
        vm.displayCensusData();
        vm.displayCountySectorData();
        }
        else {
        vm.noResultsFound = true;
        }
    });
  };
  
  /**
   * Parse the county name from the results of the county FIPS service
   */
  vm.parseCountyName = function(data) {
    vm.selectedCountyFips = data.features[0].attributes.STCN;
    vm.selectedCountyName = data.features[0].attributes.CNTY_NAME;
    vm.selectedCountyState = data.features[0].attributes.STAB;
  };
  
  /*
  Display the national sector data upon opening the page
  */
  vm.displayNationalSectorData = function() {
    Flight.getNationalSectorData()
			.success(function(data) {
        vm.displaySectorChart(data);
			});
  };
  
  /**
  Get the county greenhouse gas sector data based on the FIPS code and then make the call to display the information
  */
  vm.displayCountySectorData = function() {
    $loading.start("sectors");
    Flight.getCountySectorData(vm.selectedCountyFips)
			// promise
			.success(function(data) {
        vm.displaySectorChart(data);
			})
      .finally(function(data) {
        $loading.finish("sectors");
      });
  };
  
  /**
   * Display the sector doughnut chart
   */
  vm.displaySectorChart = function(data) {
    vm.sectorChartLabels = [];
        vm.sectorChartData = [];
        
        var noDataFound = true;
        // add the data to the doughnut chart
        if(data !== undefined && data.dataValues !== undefined) {
            // if each sector has 0 for a value, then no data was found and it should be flagged so a message can be displayed
            _.forEach(data.dataValues, function(n) {
            if(n.value !== undefined && n.value !== 0) {
                noDataFound = false;
            }
            vm.sectorChartLabels.push(n.name);
            vm.sectorChartData.push(n.value);
            
            if (noDataFound) {
                vm.noSectorDataFound = true;
            }
            else {
                vm.noSectorDataFound = false;
            }
            });
            
            vm.chartLabel = sectorChartTitle;
            vm.chartFooter = sectorChartFooter;
        }
        if(data !== undefined && data.units !== undefined) {
            vm.sectorUnits = data.units;
        }
  };
  
  /*
  * when a sector is chosen, filter and display the top facilities for the sector in the selected area
  */
  vm.filterSectorChart = function(e) {
      if(e.length > 0) {
        // get the selected sector from the doughnut chart
        var selectedSector = e[0].label;
        vm.selectedSector = selectedSector;
        vm.viewingFacilities = true;
        
        vm.chartFooter = facilityChartFooter;
        vm.facilityChartLabels = [];
        vm.facilityChartData = [[], []];
        
        $loading.start("facilities");
        // if currently viewing national data, get the top national facilities
        if(vm.isNational === true) {
          Flight.getNationalSectorTopFacilitiesData(selectedSector)
            .success(function(data) {
              // show the results in the top facilities chart
              vm.displayTopFacilitiesChart(data, selectedSector);
            })
            .finally(function(data) {
              $loading.finish("facilities");
            });
        }
        // otherwise get the top facilities for the selected county and sector 
        else {
          Flight.getCountySectorTopFacilitiesData(vm.selectedCountyFips, selectedSector)
            .success(function(data) {
              // show the results in the top facilities chart
              vm.displayTopFacilitiesChart(data, selectedSector);
            })
            .finally(function(data) {
              $loading.finish("facilities");
            });
        }
    }
  };
  
  /**
   * Display the bar chart of the top facilities
   */
  vm.displayTopFacilitiesChart = function(data, selectedSector) {
    vm.chartLabel = "Top Facilities for " + selectedSector;
    var maxNameLength = 20;
    if(data !== undefined && data.facilityData !== undefined) {
        // for display purposes, cut down the name of the facilities to at most 20 characters
        _.forEach(data.facilityData, function(n) {
        var facility = n.facility;
        if(facility.length > maxNameLength) {
            facility = facility.substr(0, maxNameLength) + "...";
        }
        // add the facility data to the chart
        vm.facilityChartLabels.push(facility);
        vm.facilityChartData[0].push(n.totalNumber);
        });
    }
    
    if(data !== undefined && data.units !== undefined) {
        vm.facilityUnits = data.units;
    }
  };
  
  /**
   * Get the census information from the current latitude and longitude. 
   * Get the census data based on the current county FIPS.
   */
  vm.displayCensusData = function() {
    if (vm.searchLatitude !== undefined && vm.searchLongitude !== undefined) {
      $loading.start("census");
      // get the current county name from the latitude and longitude
      Flight.getReverseGeocode(vm.searchLatitude, vm.searchLongitude)
      .success(function(data) {
        vm.parseCensusCountyName(data);
      });
      // get the census data for the county based on the FIPS
      Flight.getCensusData(vm.selectedCountyFips)
      .success(function(data) {
        vm.parseCensusData(data);
      })
      .finally(function() {
        $loading.finish("census");
      });
    }
  };
  
  /**
   * Parse the county name and state from the reverse geocode return data
   */
  vm.parseCensusCountyName = function(data) {
    if(data !== undefined && data.address !== undefined) {
      vm.censusCity = data.address.City;
      vm.censusState = data.address.Region;
    }
  }; 
  
  /**
   * Parse the census data for currently searched county 
   */
  vm.parseCensusData = function(data) {
    if(data !== undefined && data.features !== undefined &&
        data.features.length > 0 && data.features[0].attributes !== undefined) {
            // census data
            vm.censusTotalPopulation = getCensusValue(data.features[0].attributes.TOTALPOP, false);
            vm.censusNumberHouseholds = getCensusValue(data.features[0].attributes.HSHOLDS, false);
            vm.censusMinority = getCensusValue(data.features[0].attributes.PERC_MINOR, true);
            vm.censusEduLessThan12G = getCensusValue(data.features[0].attributes.PERC_BELOW12, true);
            vm.censusHSDiploma = getCensusValue(data.features[0].attributes.PERC_HS_DG, true);
            vm.censusBS = getCensusValue(data.features[0].attributes.PERC_COL_DG, true);
            vm.censusLess5Yr = getCensusValue(data.features[0].attributes.PERC_AGE_UNDER5, true);
            vm.censusLess18Yr = getCensusValue(data.features[0].attributes.PERC_AGE_UNDER18, true);
            vm.censusGreater64Yr = getCensusValue(data.features[0].attributes.PERC_AGE_OVER64, true);
            vm.censusHomeBefore1950 = getCensusValue(data.features[0].attributes.PERC_HOME_PRE50, true);
            vm.censusLessEnglish = getCensusValue(data.features[0].attributes.PERC_LAN_ENG_LTW, true);
            vm.censusFemale = getCensusValue(data.features[0].attributes.PERC_FEMALE, true);
            vm.censusRenters = getCensusValue(data.features[0].attributes.PERC_RENTERS, true);
      }
  }; 
  
  /**
   * Retrieve the national census information and then parse the data
   */
  vm.displayNationalCensusData = function() {
      $loading.start("census");
      
      // get the national census data
      Flight.getNationalCensusData()
      .success(function(data) {
        vm.parseNationalCensusData(data);
      })
      .finally(function() {
        $loading.finish("census");
      });
  };
  
  /**
   * Parse the national census data and set to scope variables
   */
  vm.parseNationalCensusData = function(data) {
    if(data !== undefined) {
          // census data
          vm.censusTotalPopulation = getCensusValue(data.TOTALPOP, false);
          vm.censusNumberHouseholds = getCensusValue(data.HSHOLDS, false);
          vm.censusMinority = getCensusValue(data.PERC_MINOR, true);
          vm.censusEduLessThan12G = getCensusValue(data.PERC_BELOW12, true);
          vm.censusHSDiploma = getCensusValue(data.PERC_HS_DG, true);
          vm.censusBS = getCensusValue(data.PERC_COL_DG, true);
          vm.censusLess5Yr = getCensusValue(data.PERC_AGE_UNDER5, true);
          vm.censusLess18Yr = getCensusValue(data.PERC_AGE_UNDER18, true);
          vm.censusGreater64Yr = getCensusValue(data.PERC_AGE_OVER64, true);
          vm.censusHomeBefore1950 = getCensusValue(data.PERC_HOME_PRE50, true);
          vm.censusLessEnglish = getCensusValue(data.PERC_LAN_ENG_LTW, true);
          vm.censusFemale = getCensusValue(data.PERC_FEMALE, true);
          vm.censusRenters = getCensusValue(data.PERC_RENTERS, true);
    }
  };
  
  /**
   * Given a value from the census service, this function parses the value and formats it to a decimal formatted number or percentage
   */
  function getCensusValue(dataValue, isPercentage) {
    if(dataValue === "N/A") {
      return dataValue;
    }
    
      dataValue = Math.round(dataValue);
      if(isPercentage) {
        return dataValue.toFixed().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, '$1,') + "%";
      }
      else {
        return dataValue.toFixed().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, '$1,');
      }    
  }
  
  /**
   * As a new search is performed, the heatMapController is called to update center of the heat map and reset the zoom level
   */
  vm.updateHeatMap = function () {
    $timeout(function() {
      var heatmapControllerScope = angular.element(document.getElementById("heatMapCtrl")).scope();
      heatmapControllerScope.$apply(heatmapControllerScope.center.lat = vm.searchLatitude);
      heatmapControllerScope.$apply(heatmapControllerScope.center.lng = vm.searchLongitude);
      heatmapControllerScope.$apply(heatmapControllerScope.center.zoom = 9);
     });
  };
  
  // On view load, display the national data unless a search query was specified which will mean that a county query will automatically be performed.
  if(initialLoad === true) {
    initialLoad = false;
    if(initialSearch !== undefined && initialSearch.length > 0) {
      vm.searchText = initialSearch;
      vm.getGeocode();
    }
    else {
      vm.displayNationalSectorData();
      vm.displayNationalCensusData();
    }
  }
}]);