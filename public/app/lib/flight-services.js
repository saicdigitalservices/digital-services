angular.module('authService', [])
// ===================================================
// auth factory to login and get information
// inject $http for communicating with the API
// inject $q to return promise objects
// inject AuthToken to manage tokens
// ===================================================
.factory('Auth', ["$http", "$q", "AuthToken", function($http, $q, AuthToken) {

	// create auth factory object
	var authFactory = {};

	// log a user in
	authFactory.login = function(username, password) {
		// return the promise object and its data
		return $http.post('/flight/authenticate', {
			username: username,
			password: password
		})
			.success(function(data) {
				AuthToken.setToken(data.token);
       			return data;
			});
	};

	// log a user out by clearing the token
	authFactory.logout = function() {
		// clear the token
		AuthToken.setToken();
	};

	// check if a user is logged in
	// checks if there is a local token
	authFactory.isLoggedIn = function() {
		if (AuthToken.getToken()) 
			return true;
		else
			return false;	
	};
	// return auth factory object
	return authFactory;

}])

// ===================================================
// factory for handling tokens
// inject $window to store token client-side
// ===================================================
.factory('AuthToken', ["$window", function($window) {

	var authTokenFactory = {};

	// get the token out of local storage
	authTokenFactory.getToken = function() {
		return $window.localStorage.getItem('token');
	};

	// function to set token or clear token
	// if a token is passed, set the token
	// if there is no token, clear it from local storage
	authTokenFactory.setToken = function(token) {
		if (token)
			$window.localStorage.setItem('token', token);
	 	else
			$window.localStorage.removeItem('token');
	};

	return authTokenFactory;

}])

// ===================================================
// application configuration to integrate token into requests
// ===================================================
.factory('AuthInterceptor', ["$q", "$location", "AuthToken", function($q, $location, AuthToken) {

	var interceptorFactory = {};

	// this will happen on all HTTP requests
	interceptorFactory.request = function(config) {

		// grab the token
		var token = AuthToken.getToken();

		// if the token exists, add it to the header as x-access-token
		if (token) 
			config.headers['x-access-token'] = token;
		
		return config;
	};

	// happens on response errors
	interceptorFactory.responseError = function(response) {

		// if our server returns a 403 forbidden response
		if (response.status == 403) {
			AuthToken.setToken();
			$location.path('/');
		}

		// return the errors from the server as a promise
		return $q.reject(response);
	};

	return interceptorFactory;
	
}]);
/**
 * Flight service is a service that creates a factory to retrieve the data for queries to Greenhouse Gas, ArcGIS, EJSCREEN services
 * $http added to communicate with the services
 */
angular.module("flightService", [])
	.factory("Flight", ["$http", function($http) {
		
		var flightFactory = {};
		
		/**
		 * Get the geocode information for a supplied query
		 * searchText is either a city and state or county and state
		 */
		flightFactory.getGeocode = function(searchText) {
			return $http.get("http://geocode.arcgis.com/arcgis/rest/services/World/GeocodeServer/find?text=" + searchText + "&maxLocations=6&outSR=4326&f=json");	
		};
		
		/**
		 * Given a latitude and longitude, get the address and other relevant information
		 */
		flightFactory.getReverseGeocode = function(latitude, longitude) {
			return $http.get("http://geocode.arcgis.com/arcgis/rest/services/World/GeocodeServer/reverseGeocode?location=" + longitude + "," + latitude + "&outSR=&f=json");	
		};
		
		/**
		 * Get the county name, state, and FIPS code for a location based on the latitude and longitude provided
		 */
		flightFactory.getCountyFIPS = function(latitude, longitude) {
			return $http.get("https://map11.epa.gov/arcgis/rest/services/EMEF/Boundaries/MapServer/5/query?f=json&returnGeometry=false&spatialRel=esriSpatialRelIntersects&maxAllowableOffset=19&geometry=" + longitude + "," + latitude + "&geometryType=esriGeometryPoint&inSR=4326&outFields=STCN,CNTY_NAME,STAB");
		};
		
		/**
		 * Get the census information for a county given a FIPS code for the county
		 */
		flightFactory.getCensusData = function(countyFIPS) {
			return $http.get("/flight/getCensusData?county=" + countyFIPS);
		};
		
		/**
		 * Get the census data for the entire nation
		 */
		flightFactory.getNationalCensusData = function() {
			return $http.get("/flight/getNationalCensusData");
		};
		
		/**
		 * Get the data for the national heat map
		 */ 
		flightFactory.getHeatmapData = function() {
			return $http.get("/data/heatmap");
		};
		
		/**
		 * Get the national sector emission totals
		 */
		flightFactory.getNationalSectorData = function() {
			return $http.get("/flight/getGHGSectors");
		};
		
		/**
		 * Get the sector emission totals for a county based on the FIPS code for the county
		 */
		flightFactory.getCountySectorData = function(countyFIPS) {
			return $http.get("/flight/getGHGSectors?county=" + countyFIPS);
		};
		
		/**
		 * Get the 5 highest facilities based on emissions totals for a specified sector in the nation
		 */
		flightFactory.getNationalSectorTopFacilitiesData = function(sector) {
			return $http.get("/flight/getSectorTopFacilities?sector=" + sector);
		};
		
		/**
		 * Get the 5 highest facilities based on emissions totals for a specified sector in a county specified by the FIPS code
		 */
		flightFactory.getCountySectorTopFacilitiesData = function(countyFIPS, sector) {
			return $http.get("/flight/getSectorTopFacilities?county=" + countyFIPS + "&sector=" + sector);
		};
		
		return flightFactory;
	}]);