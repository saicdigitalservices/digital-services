/**
 * Flight service is a service that creates a factory to retrieve the data for queries to Greenhouse Gas, ArcGIS, EJSCREEN services
 * $http added to communicate with the services
 */
angular.module("flightService", [])
	.factory("Flight", ["$http", function($http) {
		
		var flightFactory = {};
		
		/**
		 * Get the geocode information for a supplied query
		 * searchText is either a city and state or county and state
		 */
		flightFactory.getGeocode = function(searchText) {
			return $http.get("http://geocode.arcgis.com/arcgis/rest/services/World/GeocodeServer/find?text=" + searchText + "&maxLocations=6&outSR=4326&f=json");	
		};
		
		/**
		 * Given a latitude and longitude, get the address and other relevant information
		 */
		flightFactory.getReverseGeocode = function(latitude, longitude) {
			return $http.get("http://geocode.arcgis.com/arcgis/rest/services/World/GeocodeServer/reverseGeocode?location=" + longitude + "," + latitude + "&outSR=&f=json");	
		};
		
		/**
		 * Get the county name, state, and FIPS code for a location based on the latitude and longitude provided
		 */
		flightFactory.getCountyFIPS = function(latitude, longitude) {
			return $http.get("https://map11.epa.gov/arcgis/rest/services/EMEF/Boundaries/MapServer/5/query?f=json&returnGeometry=false&spatialRel=esriSpatialRelIntersects&maxAllowableOffset=19&geometry=" + longitude + "," + latitude + "&geometryType=esriGeometryPoint&inSR=4326&outFields=STCN,CNTY_NAME,STAB");
		};
		
		/**
		 * Get the census information for a county given a FIPS code for the county
		 */
		flightFactory.getCensusData = function(countyFIPS) {
			return $http.get("/flight/getCensusData?county=" + countyFIPS);
		};
		
		/**
		 * Get the census data for the entire nation
		 */
		flightFactory.getNationalCensusData = function() {
			return $http.get("/flight/getNationalCensusData");
		};
		
		/**
		 * Get the data for the national heat map
		 */ 
		flightFactory.getHeatmapData = function() {
			return $http.get("/data/heatmap");
		};
		
		/**
		 * Get the national sector emission totals
		 */
		flightFactory.getNationalSectorData = function() {
			return $http.get("/flight/getGHGSectors");
		};
		
		/**
		 * Get the sector emission totals for a county based on the FIPS code for the county
		 */
		flightFactory.getCountySectorData = function(countyFIPS) {
			return $http.get("/flight/getGHGSectors?county=" + countyFIPS);
		};
		
		/**
		 * Get the 5 highest facilities based on emissions totals for a specified sector in the nation
		 */
		flightFactory.getNationalSectorTopFacilitiesData = function(sector) {
			return $http.get("/flight/getSectorTopFacilities?sector=" + sector);
		};
		
		/**
		 * Get the 5 highest facilities based on emissions totals for a specified sector in a county specified by the FIPS code
		 */
		flightFactory.getCountySectorTopFacilitiesData = function(countyFIPS, sector) {
			return $http.get("/flight/getSectorTopFacilities?county=" + countyFIPS + "&sector=" + sector);
		};
		
		return flightFactory;
	}]);