// inject ngRoute for all our routing needs
angular.module('routerRoutes', ['ngRoute'])
	// configure routes
	.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
		$routeProvider
			// a user goes to search page with a query specified in the browser
			.when('/q=:search', {
				templateUrl : '/app/views/search.html',
				controller : 'searchController',
				controllerAs: 'search'				
			})
			// route for the search page
			.when('/', {
				templateUrl : '/app/views/search.html',
				controller : 'searchController',
				controllerAs: 'search'
			})
			// route for the heatmap page
			.when('/heatmap', {
				templateUrl: '/app/views/heatMap.html',
				controller : 'heatMapController',
				controllerAs: 'heatMap'
			})
			// the fall back is to go to the search page
			.otherwise({
				redirectTo: "/"
			});

	// set our app up to have pretty URLS
	$locationProvider.html5Mode(true);
}]);