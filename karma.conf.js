// Karma configuration
// Generated on Thu Dec 10 2015 08:14:18 GMT-0500 (Eastern Standard Time)

module.exports = function(config) {
  config.set({

    // base path that will be used to resolve all patterns (eg. files, exclude)
    basePath: '',


    // frameworks to use
    // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
    frameworks: ['jasmine'],


    // list of files / patterns to load in the browser
    files: [
      'http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js',
      'http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular-route.js',
      'http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular-mocks.js',
      "http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js",
      "public/assets/js/matchMedia.js",
      "public/assets/js/matchmedia-ng.js",
      "public/assets/js/leaflet.js",
      "http://cdn.jsdelivr.net/leaflet.esri/1.0.0/esri-leaflet.js",
      "public/assets/js/leaflet-heat.js",
      "public/assets/js/angular-leaflet-directive.min.js",
      "http://code.jquery.com/jquery-2.1.4.min.js",
      "http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js",
      "http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js",
      "public/assets/js/animo.js",
      "http://cdnjs.cloudflare.com/ajax/libs/jQuery-slimScroll/1.3.7/jquery.slimscroll.js",
      "http://cdnjs.cloudflare.com/ajax/libs/Chart.js/1.0.2/Chart.min.js",
      "http://cdnjs.cloudflare.com/ajax/libs/angular-chart.js/0.8.8/angular-chart.min.js",
      "http://cdn.rawgit.com/angular-ui/bower-ui-grid/master/ui-grid.min.js",
      "http://cdnjs.cloudflare.com/ajax/libs/spin.js/1.2.7/spin.min.js",
      "http://rawgithub.com/darthwade/angular-loading/master/angular-loading.min.js",
      "public/assets/js/angularjs-geolocation.min.js",
      "public/assets/js/chart.js",
      "public/assets/js/lodash.min.js",
      'public/app/tests/*.js',
      'public/app/*.js',
      'public/app/lib/*.js'
    ],


    // list of files to exclude
    exclude: [
    ],


    // preprocess matching files before serving them to the browser
    // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
    preprocessors: {
    },


    // test results reporter to use
    // possible values: 'dots', 'progress'
    // available reporters: https://npmjs.org/browse/keyword/karma-reporter
    reporters: ['progress'],


    // web server port
    port: 9876,


    // enable / disable colors in the output (reporters and logs)
    colors: true,


    // level of logging
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_INFO,


    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: true,


    // start these browsers
    // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
    browsers: ['Chrome'],


    // Continuous Integration mode
    // if true, Karma captures browsers, runs the tests and exits
    singleRun: false,

    // Concurrency level
    // how many browser should be started simultanous
    concurrency: Infinity
  })
}
