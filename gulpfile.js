var gulp = require('gulp');
var gutil = require('gulp-util');
var jshint = require("gulp-jshint");
var ngAnnotate = require('gulp-ng-annotate');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var rename = require("gulp-rename");
var minifyCss = require('gulp-minify-css');
var jasmine = require('gulp-jasmine');

var Server = require('karma').Server;

gulp.task('default', ["controllerWatch", "serviceWatch", "appWatch", "cssWatch", "jshintWatch", "watch"], function() {
	return gutil.log("Gulp is running!");
 });

// configure jshint
gulp.task("jshint", function() {
	gutil.log("JSHint is running!");
	return gulp.src(["public/app/*.js",
		"public/app/controllers/*.js",
		"public/app/services/*.js",
		"app/**/*.js"])
		.pipe(jshint())
		.pipe(jshint.reporter("jshint-stylish"));
});

 /**
 * Watch for javascript files that may be changed
 */
gulp.task("jshintWatch", function() {
	gutil.log("JSHintWatch is running!");
	gulp.watch(["public/app/**/*.js", "app/**/*.js"], ["jshint"]);
});

/**
 * Watch for javascript files that may be changed
 */
gulp.task("watch", function() {
	gutil.log("Watch is running");
	gulp.watch(["public/app/*.js",
		"public/app/controllers/*.js",
		"public/app/services/*.js",
		"public/assets/css/*.css"], ["appWatch", "controllerWatch", "serviceWatch", "cssWatch"]);
});

// clean up angular controllers and deploy to js folder
gulp.task("controllerWatch", function() {
	gutil.log("Cleaning controllers");
	gulp.src(["public/app/controllers/*.js"])            // Read the files
	.pipe(concat("flight-controller.js"))
	.pipe(ngAnnotate())					// annotate and make sure it can be minified
    .pipe(gulp.dest("public/app/lib"))            // Write non-minified to disk
	.pipe(uglify())                     // Minify
    .pipe(rename({extname: ".min.js"})) // Rename to ng-quick-date.min.js
    .pipe(gulp.dest("public/app/lib"));            // Write minified to disk
});

// clean up angular services and deploy to js folder
gulp.task("serviceWatch", function() {
	gutil.log("Cleaning services");
	gulp.src(["public/app/services/*.js"])            // Read the files
	.pipe(concat("flight-services.js"))
	.pipe(ngAnnotate())					// annotate and make sure it can be minified
    .pipe(gulp.dest("public/app/lib"))            // Write non-minified to disk
	.pipe(uglify())                     // Minify
    .pipe(rename({extname: ".min.js"})) // Rename to ng-quick-date.min.js
    .pipe(gulp.dest("public/app/lib"));            // Write minified to disk
});

// clean up angular services and deploy to js folder
gulp.task("appWatch", function() {
	gutil.log("Cleaning app files");
	gulp.src(["public/app/*.js"])            // Read the files
	.pipe(concat("flight-app.js"))
	.pipe(ngAnnotate())					// annotate and make sure it can be minified
    .pipe(gulp.dest("public/app/lib"))            // Write non-minified to disk
	.pipe(uglify())                     // Minify
    .pipe(rename({extname: ".min.js"})) // Rename to ng-quick-date.min.js
    .pipe(gulp.dest("public/app/lib"));            // Write minified to disk
});

// clean up css and deploy to lib folder
gulp.task("cssWatch", function() {
	gutil.log("Cleaning css files");
	gulp.src(["public/assets/css/app.css", "public/assets/css/theme-g.css"])            // Read the files
	.pipe(concat("flight.css"))
    .pipe(gulp.dest("public/app/lib"))            // Write non-minified to disk
	.pipe(minifyCss({compatibility: 'ie8'}))                     // Minify
    .pipe(rename({extname: ".min.css"})) // Rename to ng-quick-date.min.js
    .pipe(gulp.dest("public/app/lib"));            // Write minified to disk
});

/**
 * Run karma once and exit
 */
gulp.task('karma', function (done) {
	gutil.log("In Karma test");
  new Server({
    configFile: __dirname + '/karma.conf.js',
    singleRun: true
  }, done).start();
});

gulp.task('runUnitTests', function() {
	gutil.log('Running unit test suite');
	return gulp.src('spec/*Spec.js').pipe(jasmine());
});
gulp.task('unitTestsWatch', function() {
	gulp.watch(['spec/*Spec.js','app/*/*.js','config.js','server.js'], ['runUnitTests']);
});

gulp.task('unitTests', ['runUnitTests','unitTestsWatch']);



