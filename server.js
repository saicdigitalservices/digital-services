var express    = require('express');		// call express
var app        = express(); 				// define our app using express
var bodyParser = require('body-parser'); 	// get body-parser
var morgan     = require('morgan'); 		// used to see requests
var config 	   = require('./config');
var path 	   = require('path');
var jwt 	   = require('jsonwebtoken');


/**
 *
 * DATABASE SETUP
 *
 *
 */
var Db = require('./app/services/mongoService');


// APP CONFIGURATION ==================
// ====================================
// use body parser so we can grab information from POST requests
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// configure our app to handle CORS requests
app.use(function(req, res, next) {
	res.setHeader('Access-Control-Allow-Methods', 'GET, POST');
	res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type, Authorization');
	next();
});

// log all requests to the console 
app.use(morgan('dev'));

// set static files location
// used for requests that our frontend will make
app.use(express.static(__dirname + '/public'));

// FLIGHT ROUTES ------------------------
var flightRoutes = require('./app/routes/flight') (app, express);
app.use('/flight', flightRoutes);

// MAIN CATCHALL ROUTE --------------- 
// SEND USERS TO FRONTEND ------------
// has to be registered after FLIGHT ROUTES

// route to get the heatmap data from the mongo database 
app.get('/data/heatmap', function(req,res) {
	Db.loadHeatmapData().then(function(results) {
		results.body.toArray(function (err, docs){
			res.send(docs);
		});
	});
});

// the default route to send people to the front page for the angular application 
app.get('*', function(req, res) {
	res.sendFile(path.join(__dirname + '/public/index.html'));
});

// START THE SERVER
// ====================================
app.listen(config.web.port);